#!/bin/sh
set -e

INSTALL_DIR=$1

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")     # path where this script is located in

mkdir -p ${INSTALL_DIR}
cp -ruv ${SCRIPTPATH}/* ${INSTALL_DIR}