# silecs-codegen

This component of the SILECS PLC-framework is used in order to generate and manage silecs-project.

## Getting Started

Please check the lab-specific SILECS-Wikis for more information:

[CERN SILECS Wiki Page][CERN_Wiki]

[GSI SILECS Wiki Page][GSI_Wiki]

## License

Licensed under the GNU GENERAL PUBLIC LICENSE Version 3. See the [LICENSE file][license] for details.

[license]: LICENSE
[CERN_Wiki]: https://wikis.ch/display/SIL/SILECs+Home
[GSI_Wiki]: https://www-acc.gsi.de/wiki/Frontend/SILECS

## Dependencies

### Eclipse Plugins
Install Eclipse plug-in development environment – repo: Official Eclipse repo 
install eclipse-core-tool – http://eclipse.org/eclipse/platform-core/updates

### Maven
Configure the Eclipe-Maven:
https://www-acc.gsi.de/wiki/bin/viewauth/IN/Maven

Goto project-folder in console and run:
“mvn install”
Right click on project in eclipse configure → Convert to Maven Project
Goto eclipse and run project-->Build

## Execution

Goto eclipse-perspective
double-click plugin.xml
Launch an Eclipse Application