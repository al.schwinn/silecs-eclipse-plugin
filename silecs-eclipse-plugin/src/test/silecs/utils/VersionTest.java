package silecs.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import silecs.model.exception.SilecsException;
import silecs.utils.Version;

public class VersionTest
{
    @Test
    public void constructor_DEV() throws SilecsException
    {
        Version version = new Version("DEV");
        assertEquals(version.toString(), "DEV");
    }
    
    @Test(expected = SilecsException.class)
    public void constructor_MissingDot1() throws SilecsException
    {
        new Version("wrong");
    }
    
    @Test(expected = SilecsException.class)
    public void constructor_MissingDot2() throws SilecsException
    {
        new Version("1.wrong");
    }
    
    @Test(expected = SilecsException.class)
    public void constructor_ConvertFail() throws SilecsException
    {
        new Version("1.w.rong");
    }
    
    @Test
    public void toString_() throws SilecsException
    {
        String versionString= new String("1.2.3");
        Version version = new Version(versionString);
        assertEquals(version.toString(), versionString);
    }
    
    @Test
    public void toString_underscoredTinyAsX() throws SilecsException
    {
        Version version = new Version("1.2.3");
        assertEquals(version.toString_underscored_TinyAsX(), "1_2_x");
    }
    
    @Test
    public void comparison() throws SilecsException
    {
        Version version1 = new Version("1.2.3");
        Version version2 = new Version("1.2.99");
        assertTrue(version1.isOlderThan(version2));
        
        Version version3 = new Version("1.2.3");
        Version version4 = new Version("1.3.3");
        assertTrue(version3.isOlderThan(version4));
        
        Version version5 = new Version("1.2.3");
        Version version6 = new Version("2.2.3");
        assertTrue(version5.isOlderThan(version6));
        
        Version version7 = new Version("1.2.3");
        Version version8 = new Version("1.2.3");
        assertTrue(version7.isOlderOrEqualThan(version8));
        assertTrue(version7.equals(version8));
        
        Version version9 = new Version("1.2.77");
        Version version10 = new Version("1.2.3");
        assertTrue(version9.isNewerThan(version10));
    }
    
    @Test
    public void getEqualOrOlder() throws SilecsException
    {
        List<Version> versions = new ArrayList<Version>();
        versions.add(new Version("1.2.3"));
        versions.add(new Version("1.2.4"));
        versions.add(new Version("1.3.0"));
        versions.add(new Version("1.3.1"));
        
        Version version0 = new Version("1.2.1");
        assertEquals(null,version0.getEqualOrOlder(versions));
        
        Version version1 = new Version("1.3.1");
        assertEquals("1.3.1",version1.getEqualOrOlder(versions).toString());
        
        Version version2 = new Version("1.2.9");
        assertEquals("1.2.4",version2.getEqualOrOlder(versions).toString());
        
        Version version3 = new Version("1.4.0");
        assertEquals("1.3.1",version3.getEqualOrOlder(versions).toString());
    }
    
    @Test
    public void increment() throws SilecsException
    {
        Version version = new Version("1.2.3");
        version.incrementMinor();
        assertEquals("1.3.3",version.toString());
        version.incrementMajor();
        assertEquals("2.3.3",version.toString());
    }
    
    public void getNewest() throws SilecsException
    {
        List<Version> versions = new ArrayList<Version>();
        versions.add(new Version("1.2.3"));
        versions.add(new Version("1.2.4"));
        versions.add(new Version("3.3.0"));
        versions.add(new Version("14.3.1"));
        
        assertEquals("14.3.1",Version.getNewest(versions).toString());
    }
}