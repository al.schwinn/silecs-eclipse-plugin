// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import silecs.model.exception.SilecsException;
import silecs.view.console.ConsoleHandler;
import silecs.view.preferences.MainPreferencePage;
import silecs.utils.Version;

public class OSExecute
{
    public static String versionToCodegenDirectory(Version version, String codegenBaseDirectory)
    {
    	String fesaVersionUnderscored = version.toString().replaceAll("\\.", "_");
    	String fesaDirectory = codegenBaseDirectory + "/fesa/fesa_" + fesaVersionUnderscored;
    	return fesaDirectory;
    }
    
    public static String getmatchingSilecsFesaCodegen(String codegenBaseDirectory, Version silecsVersion) throws SilecsException, IOException
	{
    	List<Version> silecsSupportedFesaVersions = MainPreferencePage.readOutSilecsCodegenFesaVersions(silecsVersion);
    	Version selectedFesaVersion =  new Version(MainPreferencePage.getFESAVersion());
    	
		if( selectedFesaVersion.toString().equals("DEV"))
		{
			Version newest = Version.getNewest(silecsSupportedFesaVersions);
			ConsoleHandler.printMessage("DEV of FESA version was selected, using most recent silecs-fesa codegen version: " + newest.toString(), true);
			return versionToCodegenDirectory(newest,codegenBaseDirectory);
		}

    	Version nearest = selectedFesaVersion.getEqualOrOlder(silecsSupportedFesaVersions);
    	if( nearest == null )
    	{
    		throw new SilecsException("No matching silecs code-generation found for FESA-version '" + selectedFesaVersion + "'. Please select a more recent FESA-version in the Eclipse-Silecs preferences!");
    	}
    	
    	if( !nearest.equals(selectedFesaVersion) )
    	{
    		ConsoleHandler.printWarning("No silecs code-generation for the FESA-version '" + selectedFesaVersion.toString() + "' found. Attempt to use silecs-codegen of the older FESA-version: '" + nearest.toString() + "'", true);
    	}
    	
    	return versionToCodegenDirectory(nearest,codegenBaseDirectory);
	}
    
    
    public static void executePython(String file, String method, Version silecsVersion, String[] parameters) throws IOException, InterruptedException, SilecsException
    {   
    	String[] command = new String[3];  
    	command[0] = "python";
    	command[1] = "-c";
    	
    	String codegenBaseDirectory = MainPreferencePage.getCodeGenBasePath(silecsVersion);
    	String fesaDirectory = getmatchingSilecsFesaCodegen(codegenBaseDirectory, silecsVersion);
        
    	// Add required root directories
    	command[2] = "import sys; ";
    	command[2] += "sys.path.append('" + codegenBaseDirectory + "'); ";
    	command[2] += "sys.path.append('" + codegenBaseDirectory + "/migration'); "; 
    	command[2] += "sys.path.append('" + fesaDirectory + "'); ";
    	
    	// import file
    	command[2] += "import " + file + "; ";
    	
    	// execute method
    	command[2] += file +"." + method + "(";
    	
    	for(int i=0;i<parameters.length;i++)
	    {
    		command[2] += "'" + parameters[i] + "'";
    		if( i < parameters.length - 1)
    		{
    			command[2] += ",";
    		}
	    }
    	command[2] += ")";
    	int exitCode = executeCommand(command);
        if (exitCode != 0)
        {
            throw new SilecsException("Executing python-command '" + command[2] + "' failed. Error code: " + exitCode);
        }
    }
    
    class Environment
    {
    	String environmentVariableName;
    	
    	
    };
    
    public static int executeCommand(String... command) throws IOException, InterruptedException, SilecsException
    {
    	Map<String, String> env = new HashMap<String, String>();
    	return executeCommand(env, command);
    }
    
    public static int executeCommand(Map<String, String> environmentVariables, String... command) throws IOException, InterruptedException, SilecsException
    {
		ProcessBuilder pb = new ProcessBuilder(command);
		Map<String, String> env = pb.environment();
		env.putAll(environmentVariables);
		
		Process process = pb.start();
        process.waitFor();
        int exitCode = process.exitValue();

        BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line;
		while ((line = input.readLine()) != null)
        {
			ConsoleHandler.printMessage(line, true);
        }
        input.close();
		
        BufferedReader error = new BufferedReader(new InputStreamReader(process.getErrorStream()));

		while ((line = error.readLine()) != null)
        {
			ConsoleHandler.printError(line, true);
        }
		error.close();
		
		return exitCode;
    }
    
    public static void executeCommandDetached(Map<String, String> environmentVariables, String... command) throws IOException
    {
		ProcessBuilder pb = new ProcessBuilder(command);
		Map<String, String> env = pb.environment();
		env.putAll(environmentVariables);
		pb.start();
    }
}