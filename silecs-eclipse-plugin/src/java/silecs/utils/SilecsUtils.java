// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.utils;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.part.FileEditorInput;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import silecs.activator.Activator;
import silecs.control.core.DeployProjectNature;
import silecs.control.core.DesignProjectNature;
import silecs.model.exception.SilecsException;
import silecs.view.console.ConsoleHandler;
import silecs.view.preferences.MainPreferencePage;

public class SilecsUtils {
    
    public static final String CLASS_VERSION_MATCHER = "version";
    
    static public String userName;
    static public boolean isWindows = false;
    static {
        String system = System.getProperty("os.name");
        isWindows = system.toLowerCase().contains("win");
        userName = System.getProperty("user.name");
    }

    public static String findInBundle(String resourceToLocate) {
        try {
            URL fileURL = FileLocator.find(Platform.getBundle(Activator.PLUGIN_ID), new Path(resourceToLocate), null);
            fileURL = FileLocator.toFileURL(fileURL);
            if(isWindows)
                return fileURL.getPath().toString().substring(1);
            
            return fileURL.getPath().toString();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return null;
    }

    public static String getSilecsDesignFilePath(String workspacePath, String projectName) {
    	return workspacePath + "/" + projectName + "/" + getSilecsDesignFilePathRelative(projectName);
    }
    
    public static String getSilecsDeployFilePath(String workspacePath, String projectName) {
    	return workspacePath + "/" + projectName + "/" + getSilecsDeployFilePathRelative(projectName);
    }
    
    public static String getSilecsDesignFilePathRelative(String projectName) {
    	return "/src/" + projectName + "." + SilecsConstants.IEDESIGN_FILE;
    }
    
    public static String getSilecsDeployFilePathRelative(String projectName){
    	return "/src/" + projectName + "." + SilecsConstants.IEDEPLOY_FILE;
    }
    
    public static Boolean isSilecsDesign(IFile file)
    {
    	String filePath = file.getRawLocation().makeAbsolute().toOSString();
		String extension = "";
		int i = filePath.lastIndexOf('.');
		if (i > 0)
		    extension = filePath.substring(i+1);
		if(extension.equals(SilecsConstants.IEDESIGN_FILE) )
			return true;
		return false;
    }
    
    public static Boolean isSilecsDeploy(IFile file)
    {
    	String filePath = file.getRawLocation().makeAbsolute().toOSString();
		String extension = "";
		int i = filePath.lastIndexOf('.');
		if (i > 0)
		    extension = filePath.substring(i+1);
		if(extension.equals(SilecsConstants.IEDEPLOY_FILE) )
			return true;
		return false;
    }

    public static IFile getSilecsDesignFile(IProject project) {
        return project.getFile(getSilecsDesignFilePathRelative(project.getName()));
    }
    
    public static IFile getSilecsDeployFile(IProject project) {
        return project.getFile(getSilecsDeployFilePathRelative(project.getName()));
    }

    public static IFile getSilecsDocument(IProject project) throws Exception
    {
		if ( project.hasNature(DesignProjectNature.NATURE_ID) )
		{
			return SilecsUtils.getSilecsDesignFile(project);
		}
		else if( project.hasNature(DeployProjectNature.NATURE_ID) )
		{
			return SilecsUtils.getSilecsDeployFile(project);
		}
		else
        	 throw new Exception("The project: " + project.getName() + " is not a silecs-project");
    }
    
    public static Version getSilecsVersionFromProject(IProject project) throws SilecsException {
    	try 
    	{
    		IFile file = null;
    		if( project.hasNature(DeployProjectNature.NATURE_ID) )
    		{
    			file = getSilecsDeployFile(project);
    		}
    		else if( project.hasNature(DesignProjectNature.NATURE_ID) )
    		{
    			file = getSilecsDesignFile(project);
    		}
    		else
    		{
    			throw new SilecsException("Faild to extract project nature from: " + project.getName());
    		}
    		return getSilecsVersionFromFile(file);
    	}
    	catch(Exception ex)
    	{
    		throw new SilecsException("", ex);
    	}
    }
    
    public static Version getSilecsVersionFromFile(IFile file) throws SilecsException {
    	String filePath = "";
    	try 
    	{
    		filePath = file.getRawLocation().makeAbsolute().toOSString();
	    	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	    	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	    	Document doc = dBuilder.parse(filePath);
	    	Node root;
	    	if( isSilecsDesign(file) )
	    		root = doc.getElementsByTagName("SILECS-Design").item(0);
	    	else if( isSilecsDeploy(file) )
	    		root = doc.getElementsByTagName("SILECS-Deploy").item(0);
	    	else
	    		throw new SilecsException("Faild to extract project nature from: " + filePath );
	    	
	    	return new Version(root.getAttributes().getNamedItem("silecs-version").getTextContent());
    	}
    	catch(Exception ex)
    	{
    		ConsoleHandler.printError("Faild to extract silecs-version from: " + filePath, true);
    		ConsoleHandler.printStackTrace(ex);
    		throw new SilecsException("Faild to extract silecs-version from: '" + filePath + "' Reason:" + ex.getMessage());
    	}
    }
    
    public static String getSilecsDesignVersion(String filePath) throws SilecsException {
    	try 
    	{
	    	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	    	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	    	Document doc = dBuilder.parse(filePath);
	    	Node classNode = doc.getElementsByTagName("SILECS-Class").item(0);
	    	return classNode.getAttributes().getNamedItem(CLASS_VERSION_MATCHER).getNodeValue();
    	}
    	catch(Exception ex)
    	{
    		ConsoleHandler.printError("Faild to extract class-version from: " + filePath, true);
    		throw new SilecsException("Faild to extract class-version from: " + filePath, ex);
    	}
    }
    
    public static String getDeployVersion(String filePath) throws SilecsException {
    	try 
    	{
	    	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	    	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	    	Document doc = dBuilder.parse(filePath);
	    	Node deployNode = doc.getElementsByTagName("Deploy-Unit").item(0);
	    	return deployNode.getAttributes().getNamedItem(CLASS_VERSION_MATCHER).getNodeValue();
		}
		catch(Exception ex)
		{
			ConsoleHandler.printError("Faild to extract deploy-version from: " + filePath, true);
			throw new SilecsException("Faild to extract deploy-version from: " + filePath, ex);
		}
    }
    
    public static void openInEditor(final IFile file)
    {
    	Display.getDefault().asyncExec(new Runnable() {
    	    @Override
    	    public void run() {
    	    	IEditorDescriptor desc = PlatformUI.getWorkbench().getEditorRegistry().getDefaultEditor(file.getName());
    	    	IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
    	    	try {
    	    		page.openEditor(new FileEditorInput(file), desc.getId(),true);
    	    		//IEditorPart editor = page.openEditor(new FileEditorInput(file), desc.getId(),true);
    	    		//editor.getEditorSite().notify();
					//page.activate(editor);

				} catch (Exception e) {
			    	 ConsoleHandler.printError("Failed to open file:" + file, true);
			    	 e.printStackTrace();
				}
    	    }
    	});
    }

    	
    public static void checkSilecsVersion(IProject project) throws SilecsException
    {
    	Version version = getSilecsVersionFromProject(project);
    	if( isSilecsVersionSupported(version) == false )
    	{
    		throw new SilecsException("The old silecs version: '" + version + "' is not supported by this silecs-plugin. Please update your SILECS project to a more recent version (right-click --> SILECS Update Project)");
    	}
    }
    
    
    public static Boolean isSilecsVersionSupported(Version version)
    {
    	for(String supportedVersion: SilecsConstants.SUPPORTED_SILECS_MAJOR_VERSIONS)
    	{
    		if(version.toString().startsWith(supportedVersion))
    			return true;
    	}
    	return false;
    }
    
    public static List<String> readOutAvailableSilecsVersions()
	{
    	List<String> silecsVersions = new ArrayList<String>();
    	if (MainPreferencePage.isLocalDirectoryUsed())
    	{
    		silecsVersions.add("DEV");
    		return silecsVersions;
    	}
    	
    	// We just pick any sub-project
		File silecsBaseFolder = new File(MainPreferencePage.getGlobalSilecsDirectory() + "/silecs-model");
	    for (final File fileEntry : silecsBaseFolder.listFiles())
	    {
	    	silecsVersions.add(fileEntry.getName());
	    }
	    Collections.sort(silecsVersions);
	    Collections.reverse(silecsVersions);
	    return silecsVersions;
	}
    
    public static IFile extractSilecsFileFromEvent(ExecutionEvent event) throws Exception
	{
    	String parameter = event.getParameter("silecs-eclipse-plugin.commandParameter");
    	if ( parameter.equals("menubar"))
    	{
			IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindow(event);
			IWorkbenchPage activePage = window.getActivePage();
	
	        // Get the Selection from the active WorkbenchPage page
	        ISelection selection = activePage.getSelection();
	        if(selection instanceof ITreeSelection) // Event was triggered by right-click on project
	        {
	            TreeSelection treeSelection = (TreeSelection) selection;
	            TreePath[] treePaths = treeSelection.getPaths();
	            TreePath treePath = treePaths[0];
	
	            // The first segment should be a IProject
	            Object firstSegmentObj = treePath.getFirstSegment();
	            IProject theProject = (IProject) ((IAdaptable)firstSegmentObj).getAdapter(IProject.class);
	            if( theProject == null )
	            {
	            	ConsoleHandler.printError("Failed to find the right project", true);
	            	throw new ExecutionException("Failed to find the right project");
	            }
	            return getSilecsDocument(theProject);
	        }
    	}
    	if (parameter.equals("toolbar"))
    	{
	        // The default: the toolbar button was pressed
	    	IEditorPart editor = HandlerUtil.getActiveEditor(event);
	        if (editor == null)
	        {
	        	ConsoleHandler.printError("Failed to find active SILECS editor", true);
	        	throw new Exception("Failed to find active SILECS editor");
	        } 
	    	return ((IFileEditorInput) editor.getEditorInput()).getFile();
    	}
    	String message = new String("Failed to find correct silecs-project/file - command: '" + event.getCommand().getName() + "' send with parameter: '" + parameter + "'.");
    	ConsoleHandler.printError(message, true);
    	throw new Exception(message);
    }
}
