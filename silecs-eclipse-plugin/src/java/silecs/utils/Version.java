package silecs.utils;

import java.util.List;

import silecs.model.exception.SilecsException;

public class Version
{
    private int    major_;
    private int    minor_;
    private int    tiny_;
    private String versionString_;

    private static final int RATING_FACTOR_MAJOR = 1000000;
    private static final int RATING_FACTOR_MINOR = 100;
    private static final int RATING_FACTOR_TINY  = 1;

    private void rebuildString()
    {
        String majorString = Integer.toString(major_);
        String minorString = Integer.toString(minor_);
        String tinyString = Integer.toString(tiny_);
        versionString_ = majorString + "." + minorString + "." + tinyString;
    }

    private int compareTo(Version version)
    {
        int diff = 0;
        diff += (this.major_ - version.major_) * RATING_FACTOR_MAJOR;
        diff += (this.minor_ - version.minor_) * RATING_FACTOR_MINOR;
        diff += (this.tiny_ - version.tiny_) * RATING_FACTOR_TINY;
        return diff;
    }

    public Version(String versionString) throws SilecsException
    {
        versionString_ = versionString;
        if (versionString.equals("DEV"))
            return;
        int firstDot = versionString.indexOf(".");
        int secondDot = versionString.indexOf(".", firstDot + 1);
        if (firstDot == -1 || secondDot == -1)
        {
            throw new SilecsException("The version '" + versionString_ + "' cannot be transformed to major/minor/tiny integers");
        }
        try
        {
            major_ = Integer.parseInt(versionString.substring(0, firstDot));
            minor_ = Integer.parseInt(versionString.substring(firstDot + 1, secondDot));
            tiny_ = Integer.parseInt(versionString.substring(secondDot + 1));
        }
        catch (NumberFormatException e)
        {
            throw new SilecsException("The version '" + versionString_ + "' cannot be transformed to major/minor/tiny integers");
        }
    }

    public Version(Version originalVersion) throws SilecsException
    {
        this(originalVersion.toString());
    }

    public String toString()
    {
        return versionString_;
    }

    public String toString_underscored_TinyAsX()
    {
        if (versionString_ == "DEV")
            return versionString_;
        String majorString = Integer.toString(major_);
        String minorString = Integer.toString(minor_);
        return majorString + "_" + minorString + "_x";
    }

    public boolean isOlderThan(Version version)
    {
        return (this.compareTo(version) < 0);
    }

    public boolean isOlderOrEqualThan(Version version)
    {
        return (this.compareTo(version) <= 0);
    }

    public boolean isNewerThan(Version version)
    {
        return (this.compareTo(version) > 0);
    }

    public boolean equals(Version version)
    {
        return major_ == version.major_ && minor_ == version.minor_ && tiny_ == version.tiny_;
    }

    // returns the nearest available version
    public Version getEqualOrOlder(List<Version> versions)
    {
        int minimumDiff = Integer.MAX_VALUE;
        Version nearest = null;
        for (Version version : versions)
        {
            int diff = compareTo(version);
            if (diff < 0) // 'version' is newer than current version
                continue;
            if (diff < minimumDiff)
            {
                minimumDiff = diff;
                nearest = version;
            }
        }
        return nearest;
    }

    public void incrementMinor()
    {
        if (versionString_ == "DEV")
            return;
        this.minor_++;
        this.rebuildString();
    }

    public void incrementMajor()
    {
        if (versionString_ == "DEV")
            return;
        this.major_++;
        this.rebuildString();
    }

    public Version getNextMinor() throws SilecsException
    {
        Version next = new Version(this);
        next.incrementMinor();
        return next;
    }

    public Version getNextMajor() throws SilecsException
    {
        Version next = new Version(this);
        next.incrementMajor();
        return next;
    }

    static public Version getNewest(List<Version> versions)
    {
        Version newest = versions.get(0);
        for (Version version : versions)
        {
            if (newest.isOlderThan(version))
            {
                newest = version;
            }
        }
        return newest;
    }
}
