// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;
import org.eclipse.ui.internal.dialogs.WorkbenchWizardElement;
import org.eclipse.ui.internal.wizards.AbstractExtensionWizardRegistry;
import org.eclipse.ui.wizards.IWizardCategory;
import org.eclipse.ui.wizards.IWizardDescriptor;

import silecs.utils.SilecsConstants;

@SuppressWarnings("restriction")
public class ApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {

    public ApplicationWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
        super(configurer);
    }

    @Override
    public ActionBarAdvisor createActionBarAdvisor(IActionBarConfigurer configurer) {
        return new ApplicationActionBarAdvisor(configurer);
    }

    @Override
    public void preWindowOpen() {
        IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
        configurer.setInitialSize(new Point(700, 500));
        configurer.setShowCoolBar(true);
        configurer.setShowStatusLine(true);
        configurer.setTitle(SilecsConstants.SILECS_NAME); //$NON-NLS-1$
        configurer.setShowProgressIndicator(true);
    }

    @Override
    public void postWindowOpen() {
        super.postWindowOpen();

        addChangedEditorListener();
        
        // removing unnecessary wizards from new and import menu.
        AbstractExtensionWizardRegistry wizardRegistry = (AbstractExtensionWizardRegistry) PlatformUI.getWorkbench()
                .getNewWizardRegistry();
        IWizardCategory[] categories = PlatformUI.getWorkbench().getNewWizardRegistry().getRootCategory()
                .getCategories();

        for (IWizardDescriptor wizard : getAllWizards(categories)) {
            if (wizard.getCategory().getId().matches("org.eclipse.ui.Basic")) {
                WorkbenchWizardElement wizardElement = (WorkbenchWizardElement) wizard;
                wizardRegistry.removeExtension(wizardElement.getConfigurationElement().getDeclaringExtension(),
                        new Object[] { wizardElement });
            } else if (wizard.getCategory().getId().matches("org.eclipse.wst.xml.examples")) {
                WorkbenchWizardElement wizardElement = (WorkbenchWizardElement) wizard;
                wizardRegistry.removeExtension(wizardElement.getConfigurationElement().getDeclaringExtension(),
                        new Object[] { wizardElement });
            } else if (wizard.getCategory().getId().matches("org.eclipse.wst.XMLCategory")) {
                WorkbenchWizardElement wizardElement = (WorkbenchWizardElement) wizard;
                wizardRegistry.removeExtension(wizardElement.getConfigurationElement().getDeclaringExtension(),
                        new Object[] { wizardElement });
            } else if (wizard.getCategory().getId().matches("org.eclipse.cdt.ui.newCWizards")) {
                WorkbenchWizardElement wizardElement = (WorkbenchWizardElement) wizard;
                wizardRegistry.removeExtension(wizardElement.getConfigurationElement().getDeclaringExtension(),
                        new Object[] { wizardElement });
            }

        }

        wizardRegistry = (AbstractExtensionWizardRegistry) PlatformUI.getWorkbench().getImportWizardRegistry();

        categories = PlatformUI.getWorkbench().getImportWizardRegistry().getRootCategory().getCategories();

        for (IWizardDescriptor wizard : getAllWizards(categories)) {
            if (wizard.getCategory().getId().matches("org.eclipse.cdt.ui.importWizardCategory")) {
                WorkbenchWizardElement wizardElement = (WorkbenchWizardElement) wizard;
                wizardRegistry.removeExtension(wizardElement.getConfigurationElement().getDeclaringExtension(),
                        new Object[] { wizardElement });
            } else if (wizard.getCategory().getId().matches("org.eclipse.debug.ui")) {
                WorkbenchWizardElement wizardElement = (WorkbenchWizardElement) wizard;
                wizardRegistry.removeExtension(wizardElement.getConfigurationElement().getDeclaringExtension(),
                        new Object[] { wizardElement });
            } else if (wizard.getCategory().getId().matches("org.eclipse.wst.XMLCategory")) {
                WorkbenchWizardElement wizardElement = (WorkbenchWizardElement) wizard;
                wizardRegistry.removeExtension(wizardElement.getConfigurationElement().getDeclaringExtension(),
                        new Object[] { wizardElement });
            } else if (wizard.getCategory().getId().matches("org.eclipse.team.ui.importWizards")) {
                WorkbenchWizardElement wizardElement = (WorkbenchWizardElement) wizard;
                wizardRegistry.removeExtension(wizardElement.getConfigurationElement().getDeclaringExtension(),
                        new Object[] { wizardElement });
            }
        }
    }

    private IWizardDescriptor[] getAllWizards(IWizardCategory[] categories) {
        List<IWizardDescriptor> results = new ArrayList<IWizardDescriptor>();
        for (IWizardCategory wizardCategory : categories) {
            results.addAll(Arrays.asList(wizardCategory.getWizards()));
            results.addAll(Arrays.asList(getAllWizards(wizardCategory.getCategories())));
        }
        return results.toArray(new IWizardDescriptor[0]);
    }

    private void addChangedEditorListener() {
        IWorkbench workbench = PlatformUI.getWorkbench();
        IWorkbenchPage page = workbench.getActiveWorkbenchWindow().getActivePage();
        page.addPartListener(new IPartListener() {

            @Override
            public void partOpened(IWorkbenchPart part) {
                setApplicationTitle(); 
            }

            @Override
            public void partDeactivated(IWorkbenchPart part) {
                setApplicationTitle(); 
            }

            @Override
            public void partClosed(IWorkbenchPart part) {
                setApplicationTitle(); 
            }

            @Override
            public void partBroughtToTop(IWorkbenchPart part) {
                setApplicationTitle(); 
            }

            @Override
            public void partActivated(IWorkbenchPart part) {
                setApplicationTitle(); 
            }
            
            private void setApplicationTitle() {
//                IEditorPart editorPart = page.getActiveEditor();
//                IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
//                
//                if (editorPart != null) {
//                    IFileEditorInput editorInput = (IFileEditorInput) editorPart.getEditorInput();
//                    IFile file = editorInput.getFile();
//
//                    configurer.setTitle(file.getFullPath() + " - " + SilecsConstants.SILECS_NAME + " - "
//                            + ResourcesPlugin.getWorkspace());
//                } else {
//                    configurer.setTitle(SilecsConstants.SILECS_NAME + " - " + ResourcesPlugin.getWorkspace() );
//                }
            }
        });

    }
}
