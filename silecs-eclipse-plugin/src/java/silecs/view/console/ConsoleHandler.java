// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.console;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

public class ConsoleHandler {

    private static MessageConsole console;
    private static MessageConsoleStream messageStream;
    private static MessageConsoleStream errorStream;
    private static MessageConsoleStream warningStream;
    
    static {
        console = new MessageConsole("Console", null);
        ConsolePlugin.getDefault().getConsoleManager().addConsoles(new IConsole[] { console });

        messageStream = console.newMessageStream();
        errorStream = console.newMessageStream();
        warningStream = console.newMessageStream();

        final Display display = Display.getDefault();
        Display.getDefault().asyncExec(new Runnable()
        {
          public void run()
	        { 
		         errorStream.setColor(display.getSystemColor(SWT.COLOR_RED));
		         warningStream.setColor(display.getSystemColor(SWT.COLOR_DARK_YELLOW)); 
	        }
      });
    }

    public static void clear() {
    	console.clearConsole();
    }
    
    /**
     * Prints message in the silecs console.
     * 
     * @param message Message to appear in the console
     * @param activateOnWrite determines whether to activate the console
     */
    public static void printMessage(String message, boolean activateOnWrite) {
        messageStream.setActivateOnWrite(activateOnWrite);
        messageStream.println(message);
    }
    
    /**
     * Prints error message in the silecs console.
     * 
     * @param Error message to appear in the console
     * @param activateOnWrite determines whether to activate the console
     */
    public static void printError(String message, boolean activateOnWrite) {
        errorStream.setActivateOnWrite(activateOnWrite);
        errorStream.println(message);
    }
    

    private static void printTrace(Throwable ex) {
    	if( ex == null)
    		return;
    	StackTraceElement[] stacktrace = ex.getStackTrace();

    	     for(int i=1; i<stacktrace.length; i++){
    	    			 errorStream.println(stacktrace[i].toString());
            }
    }
    
    public static void printStackTrace(Exception ex) {
        errorStream.println(ex.getLocalizedMessage());
        errorStream.println("Caused by: ");
        printTrace(ex.getCause());
        errorStream.setActivateOnWrite(true);
    }
    
    
    /**
     * Prints error message in the silecs console.
     * 
     * @param Warning message to appear in the console
     * @param activateOnWrite determines whether to activate the console
     */
    public static void printWarning(String message, boolean activateOnWrite) {
        warningStream.setActivateOnWrite(activateOnWrite);
        warningStream.println(message);
    }

}
