// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.explorer;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.ui.views.navigator.ResourceComparator;

import silecs.control.core.DesignProjectNature;

public class ContentSorter extends ViewerComparator {

    private ResourceComparator resourceComparator;

    public ContentSorter() {
        resourceComparator = new ResourceComparator(ResourceComparator.NAME);
    }

    @Override
    public int compare(Viewer viewer, Object e1, Object e2) {
        if (e1 instanceof IProject && e2 instanceof IProject) {

            IProject f1 = (IProject) e1;
            IProject f2 = (IProject) e2;
            
            if (!f1.isOpen() || !f2.isOpen())
                return resourceComparator.compare(viewer, e1, e2);
            
            boolean f1b = false;
            boolean f2b = false;
            try {
                f1b = f1.hasNature(DesignProjectNature.NATURE_ID);
                f2b = f2.hasNature(DesignProjectNature.NATURE_ID);
            } catch (CoreException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (f1b ^ f2b)
                if (f1b == true) {
                    return -1;
                } else {
                    return 1;
                }
        }

        // else let the resource comparator do the ordering
        return resourceComparator.compare(viewer, e1, e2);
    }
}
