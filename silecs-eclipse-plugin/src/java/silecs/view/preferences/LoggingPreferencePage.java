// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.preferences;

import java.io.File;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import silecs.activator.Activator;

public class LoggingPreferencePage extends PreferencePage implements IWorkbenchPreferencePage {


    public static final String[] LOG_LEVELS = { "FATAL", "ERROR", "WARN", "INFO", "DEBUG", "TRACE", "OFF" };
    public static final int DEFAULT_LOG_LEVEL_INDEX = 3;
    public static final String DEFAULT_LOG_DIRECTORY = System.getProperty("user.home") + "/.logs/fesa-plugin";

    public static final String LOG_LEVEL = "fesaLog.level";
    public static final String LOG_DIRECTORY = "fesaLog.directory";
    public static final String BRING_CONSOLE_TO_FRONT = "CONSOLE_TO_FRONT";
    
    private Combo logLevel;
    private Text logDirectory;

    @Override
    public void init(IWorkbench workbench) {
        setPreferenceStore(Activator.getDefault().getPreferenceStore());
        setDescription("SILECS Logging");
    }

    @Override
    protected Control createContents(Composite parent) {
        IPreferenceStore prefs = getPreferenceStore();

        Composite container = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout(3, false);
        container.setLayout(layout);

        Label level = new Label(container, SWT.NONE);
        level.setText("Log Level");
        logLevel = new Combo(container, SWT.BORDER | SWT.V_SCROLL | SWT.READ_ONLY);
        logLevel.setItems(LOG_LEVELS);
        logLevel.select(prefs.getInt(LOG_LEVEL));
        GridData dataLayout = new GridData();
        dataLayout.horizontalSpan = 2;
        logLevel.setLayoutData(dataLayout);

        Label directory = new Label(container, SWT.NONE);
        directory.setText("Log Directory");
        logDirectory = new Text(container, SWT.BORDER);
        logDirectory.setText(prefs.getString(LOG_DIRECTORY));
        dataLayout = new GridData(SWT.FILL, SWT.TOP, true, false);
        logDirectory.setLayoutData(dataLayout);

        Button browse = new Button(container, SWT.PUSH);
        browse.setText("browse");
        browse.addListener(SWT.Selection, new Listener() {

            @Override
            public void handleEvent(Event event) {
                DirectoryDialog dirDialog = new DirectoryDialog(getShell());
                logDirectory.setText(dirDialog.open());
            }
        });

        return container;
    }

    @Override
    public boolean performOk() {
        IPreferenceStore prefs = getPreferenceStore();

        int levelIndex = logLevel.getSelectionIndex();
        File logDir = new File(logDirectory.getText());
        
        if (!logDir.isDirectory()) {
            setErrorMessage("Invalid Path !" + logDir.getAbsolutePath());
            return false;
        }
        
        prefs.setValue(LOG_LEVEL, levelIndex);
        prefs.setValue(LOG_DIRECTORY, logDir.getAbsolutePath());


        return true;
    }

    @Override
    protected void performDefaults() {
        logLevel.select(DEFAULT_LOG_LEVEL_INDEX);
        logDirectory.setText(DEFAULT_LOG_DIRECTORY);
    }

}
