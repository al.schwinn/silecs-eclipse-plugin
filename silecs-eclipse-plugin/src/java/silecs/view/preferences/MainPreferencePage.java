// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.preferences;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import silecs.activator.Activator;
import silecs.control.labExtension.LabExtensionProvider;
import silecs.model.exception.SilecsException;
import silecs.utils.Version;
import silecs.view.console.ConsoleHandler;

public class MainPreferencePage extends PreferencePage implements IWorkbenchPreferencePage {
    
	public static final String SILECS_DIRECTORY_TAG = "SILECS_DIRECTORY";
	public static final String FESA_DIRECTORY_TAG = "FESA_DIRECTORY";
	public static final String LOCAL_DIRECTORY_TAG = "LOCAL_DIRECTORY";
	public static final String USE_LOCAL_DIRECTORY_TAG = "USE_LOCAL_DIRECTORY";
	public static final String FESSA_VERSION_TAG = "FESA_VERSION";
	
	public static final Boolean USE_LOCAL_DIRECTORY_DEFAULT = false;
	
	private static List<Version> fesaVersions_ = getInstalledFesaVersions();
    
	private static Text silecsDirectory;
	private static Text fesaDirectory;
	private static Button useLocalWorkspace;
	private static Text localWorkspaceDirectory;
	private static Combo fesaVersion;
	
	
	public static boolean isLocalDirectoryUsed()
	{
		IPreferenceStore prefs = Activator.getDefault().getPreferenceStore();
		return prefs.getBoolean(USE_LOCAL_DIRECTORY_TAG);
	}
	
	public static String getGlobalSilecsDirectory()
	{
		IPreferenceStore prefs = Activator.getDefault().getPreferenceStore();
		return prefs.getString(SILECS_DIRECTORY_TAG);
	}
	
	public static String getModelBasePath(Version modelVersion)
	{
		IPreferenceStore prefs = Activator.getDefault().getPreferenceStore();
		if(prefs.getBoolean(USE_LOCAL_DIRECTORY_TAG))
			return prefs.getString(LOCAL_DIRECTORY_TAG) + "/silecs-model/src/xml";
		else
			return prefs.getString(SILECS_DIRECTORY_TAG) + "/silecs-model/" + modelVersion.toString() + "/" + "xml";
	}
	
	public static String getCodeGenBasePath(Version codegenVersion)
	{
		IPreferenceStore prefs = Activator.getDefault().getPreferenceStore();
		if(prefs.getBoolean(USE_LOCAL_DIRECTORY_TAG))
			return prefs.getString(LOCAL_DIRECTORY_TAG) + "/silecs-codegen/src/xml";
		else
			return prefs.getString(SILECS_DIRECTORY_TAG) + "/silecs-codegen/" + codegenVersion.toString() + "/" + "xml";
	}
	
	public static String getDiagToolBasePath(Version diagToolVersion)
	{
		IPreferenceStore prefs = Activator.getDefault().getPreferenceStore();
		if(prefs.getBoolean(USE_LOCAL_DIRECTORY_TAG))
			return prefs.getString(LOCAL_DIRECTORY_TAG) + "/silecs-diagnostic-cpp/build";
		else
			return prefs.getString(SILECS_DIRECTORY_TAG) + "/silecs-diagnostic-cpp/" + diagToolVersion.toString();
	}
	
	public static String getSilecsLibraryBasePath(Version silecsLibraryVersion)
	{
		IPreferenceStore prefs = Activator.getDefault().getPreferenceStore();
		if(prefs.getBoolean(USE_LOCAL_DIRECTORY_TAG))
			return prefs.getString(LOCAL_DIRECTORY_TAG) + "/silecs-communication-cpp/build";
		else
			return prefs.getString(SILECS_DIRECTORY_TAG) + "/silecs-communication-cpp/" + silecsLibraryVersion.toString();
	}
	
	public static String getSNAP7LibraryBasePath(Version silecsLibraryVersion)
	{
		IPreferenceStore prefs = Activator.getDefault().getPreferenceStore();
		if(prefs.getBoolean(USE_LOCAL_DIRECTORY_TAG))
			return prefs.getString(LOCAL_DIRECTORY_TAG) + "/snap7/snap7-full/build";
		else
			return prefs.getString(SILECS_DIRECTORY_TAG) + "/snap7/" + silecsLibraryVersion.toString();
	}
	
	public static String getFESAVersion() throws SilecsException
	{
		int index = Activator.getDefault().getPreferenceStore().getInt(FESSA_VERSION_TAG);
		if( index == -1 ) //no FESA-versions found on the system 
			return "NO_FESA_VERSIONS_AVAILABLE";
		return fesaVersions_.get(index).toString();
	}
	
    public static String getFESADirectory() throws SilecsException
    {
        return Activator.getDefault().getPreferenceStore().getString(FESA_DIRECTORY_TAG);
    }
    
	public static String getLocalDirectoryDefault()
	{
		return System.getProperty("user.home") + "/workspace";
	}

    @Override
    public void init(IWorkbench workbench)
    {
        setDescription("SILECS Preferences");
        setPreferenceStore(Activator.getDefault().getPreferenceStore());
    }
    
    protected void switchEnableLocalWorkspace(Boolean localEnabled)
    {
		fesaDirectory.setEnabled(true);//to stay backward -compartible
		silecsDirectory.setEnabled(!localEnabled);
		localWorkspaceDirectory.setEnabled(localEnabled);
    }

    private void updateAvailableFesaVersions(IPreferenceStore prefs)
	{
    	fesaVersions_ = getInstalledFesaVersions();
    	String[] versionStringArray = new String[fesaVersions_.size()];
    	for( int i = 0; i< fesaVersions_.size(); i++)
    		versionStringArray[i] = fesaVersions_.get(i).toString();
        fesaVersion.setItems(versionStringArray);
        fesaVersion.select(prefs.getInt(FESSA_VERSION_TAG));
	}
    
    public static List<Version> getInstalledFesaVersions()
	{
        try
        {
            LabExtensionProvider labextensionProvider = LabExtensionProvider.getLabExtensionProvider();
            fesaVersions_ = labextensionProvider.readOutAvailableFesaVersions();
        }
        catch (SilecsException e)
        {
            ConsoleHandler.printStackTrace(e);
            return new ArrayList<Version>();
        }
        return fesaVersions_;
	}
    
    public static List<Version> readOutSilecsCodegenFesaVersions(Version silecsVersion) throws SilecsException
	{
		String fesaBaseDirectory = MainPreferencePage.getCodeGenBasePath(silecsVersion) + "/fesa";
    	if(!new File(fesaBaseDirectory).exists())
    	{
    		throw new SilecsException("The directory: '" + fesaBaseDirectory + "' does not exist. Please check your Eclipse-Silecs preferences!");
    	}
		File fesaBaseFolder = new File(fesaBaseDirectory);
		
		List<Version> fesaVersions = new ArrayList<Version>();
		
		if(!fesaBaseFolder.exists())
			return fesaVersions;

	    for (final File fileEntry : fesaBaseFolder.listFiles())
	    {
	    	if(fileEntry.isDirectory())
	    	{
	    		if(fileEntry.getName().startsWith("fesa"))
	    		{
	    			String versionString = fileEntry.getName();
	    			versionString = versionString.replaceAll("fesa_", "");
	    			versionString = versionString.replaceAll("_", "\\.");
	    			Version version = new Version(versionString);
	    			fesaVersions.add(version);
	    		}
	    	}
	    }
	    return fesaVersions;
	}

    @Override
    protected Control createContents(Composite parent)
    {
        IPreferenceStore prefs = getPreferenceStore();

        Composite container = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout(3, false);
        container.setLayout(layout);

        GridData dataLayout = new GridData();
        dataLayout.horizontalSpan = 2;

        { // fesa base
	        Label directory = new Label(container, SWT.NONE);
	        directory.setText("FESA Base Path");
	        fesaDirectory = new Text(container, SWT.BORDER);
	        fesaDirectory.setText(prefs.getString(FESA_DIRECTORY_TAG));
	        dataLayout = new GridData(SWT.FILL, SWT.TOP, true, false);
	        fesaDirectory.setLayoutData(dataLayout);
	
	        Button browse = new Button(container, SWT.PUSH);
	        browse.setText("browse");
	        browse.addListener(SWT.Selection, new Listener() {
	
	            @Override
	            public void handleEvent(Event event) {
	                DirectoryDialog dirDialog = new DirectoryDialog(getShell());
	                fesaDirectory.setText(dirDialog.open());
	            }
	        });
        }
        
        {	// FESA version
	        Label level = new Label(container, SWT.NONE);
	        level.setText("FESA Version to use");
	        fesaVersion = new Combo(container, SWT.BORDER | SWT.V_SCROLL | SWT.READ_ONLY);
	        updateAvailableFesaVersions(prefs);
	        dataLayout = new GridData();
	        dataLayout.horizontalSpan = 2;
	        fesaVersion.setLayoutData(dataLayout);
        }
        
        { // silecs base
	        Label directory = new Label(container, SWT.NONE);
	        directory.setText("SILECS Base Path");
	        silecsDirectory = new Text(container, SWT.BORDER);
	        silecsDirectory.setText(prefs.getString(SILECS_DIRECTORY_TAG));
	        dataLayout = new GridData(SWT.FILL, SWT.TOP, true, false);
	        silecsDirectory.setLayoutData(dataLayout);
	
	        Button browse = new Button(container, SWT.PUSH);
	        browse.setText("browse");
	        browse.addListener(SWT.Selection, new Listener() {
	
	            @Override
	            public void handleEvent(Event event) {
	                DirectoryDialog dirDialog = new DirectoryDialog(getShell());
	                silecsDirectory.setText(dirDialog.open());
	            }
	        });
        }
        
	      useLocalWorkspace = new Button(container, SWT.CHECK);
	      useLocalWorkspace.setText("Use local workspace");
	      dataLayout = new GridData(SWT.FILL, SWT.TOP, true, false);
	      dataLayout.horizontalSpan = 3;
	      useLocalWorkspace.setLayoutData(dataLayout);
	      useLocalWorkspace.setSelection(prefs.getBoolean(USE_LOCAL_DIRECTORY_TAG));
	      useLocalWorkspace.addListener(SWT.Selection, new Listener() {
	          @Override
	          public void handleEvent(Event event) {
	          		switchEnableLocalWorkspace(useLocalWorkspace.getSelection());
	          }
	      });
        
        { // Local SILECS Workspace
	        Label directory = new Label(container, SWT.NONE);
	        directory.setText("Local SILECS Workspace");
	        localWorkspaceDirectory = new Text(container, SWT.BORDER);
	        localWorkspaceDirectory.setText(prefs.getString(LOCAL_DIRECTORY_TAG));
	        dataLayout = new GridData(SWT.FILL, SWT.TOP, true, false);
	        localWorkspaceDirectory.setLayoutData(dataLayout);
	
	        Button browse = new Button(container, SWT.PUSH);
	        browse.setText("browse");
	        browse.addListener(SWT.Selection, new Listener() {
	
	            @Override
	            public void handleEvent(Event event) {
	                DirectoryDialog dirDialog = new DirectoryDialog(getShell());
	                localWorkspaceDirectory.setText(dirDialog.open());
	            }
	        });
        }

        switchEnableLocalWorkspace(useLocalWorkspace.getSelection());
        return container;
    }
    
    @Override
    public boolean performOk()
    {
        IPreferenceStore prefs = getPreferenceStore();
        prefs.setValue(SILECS_DIRECTORY_TAG, silecsDirectory.getText());
        prefs.setValue(FESA_DIRECTORY_TAG, fesaDirectory.getText());
        prefs.setValue(USE_LOCAL_DIRECTORY_TAG, useLocalWorkspace.getSelection());
        prefs.setValue(LOCAL_DIRECTORY_TAG, localWorkspaceDirectory.getText());
        prefs.setValue(FESSA_VERSION_TAG, fesaVersion.getSelectionIndex());
        updateAvailableFesaVersions(prefs);
        return true;
    }

    @Override
    protected void performDefaults()
    {
        LabExtensionProvider labextensionProvider = null;
        try
        {
            labextensionProvider = LabExtensionProvider.getLabExtensionProvider();
        }
        catch (SilecsException e)
        {
            ConsoleHandler.printStackTrace(e);
            return;
        }
        silecsDirectory.setText(labextensionProvider.getDefaultSilecsBasePath());
        fesaDirectory.setText(labextensionProvider.getDefaultFesaBasePath());
        useLocalWorkspace.setSelection(USE_LOCAL_DIRECTORY_DEFAULT);
        localWorkspaceDirectory.setText(MainPreferencePage.getLocalDirectoryDefault());
        localWorkspaceDirectory.setEnabled(false);
        fesaVersion.select(0);
        switchEnableLocalWorkspace(useLocalWorkspace.getSelection());
    }

}
