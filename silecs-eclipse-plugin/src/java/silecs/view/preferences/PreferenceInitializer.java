// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.preferences;


import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import silecs.activator.Activator;
import silecs.control.labExtension.LabExtensionProvider;
import silecs.model.exception.SilecsException;
import silecs.view.console.ConsoleHandler;


public class PreferenceInitializer extends AbstractPreferenceInitializer {
    
    @Override
    public void initializeDefaultPreferences() {
        IPreferenceStore prefs = Activator.getDefault().getPreferenceStore();
        
        try
        {
            LabExtensionProvider labextEnsionProvider = LabExtensionProvider.getLabExtensionProvider();
            prefs.setDefault(MainPreferencePage.SILECS_DIRECTORY_TAG, labextEnsionProvider.getDefaultSilecsBasePath());
            prefs.setDefault(MainPreferencePage.FESA_DIRECTORY_TAG, labextEnsionProvider.getDefaultFesaBasePath());
            prefs.setDefault(MainPreferencePage.LOCAL_DIRECTORY_TAG, MainPreferencePage.getLocalDirectoryDefault());
            prefs.setDefault(MainPreferencePage.USE_LOCAL_DIRECTORY_TAG, MainPreferencePage.USE_LOCAL_DIRECTORY_DEFAULT);
        }
        catch (SilecsException e)
        {
            ConsoleHandler.printStackTrace(e);
        }

        
        // init the xml editor preferences
//        prefs.setDefault(EditorPreferencePage.VALIDATE_ON_SAVE, true);
//        
//        RGB dflt = EditorPreferencePage.DEFAULT_EDITABLE;
//        prefs.setDefault(EditorPreferencePage.EDITABLE_COLOR_R, dflt.red);
//        prefs.setDefault(EditorPreferencePage.EDITABLE_COLOR_G, dflt.green);
//        prefs.setDefault(EditorPreferencePage.EDITABLE_COLOR_B, dflt.blue);
//        
//        dflt = EditorPreferencePage.DEFAULT_CONTENT;
//        prefs.setDefault(EditorPreferencePage.CONTENT_COLOR_R, dflt.red);
//        prefs.setDefault(EditorPreferencePage.CONTENT_COLOR_G, dflt.green);
//        prefs.setDefault(EditorPreferencePage.CONTENT_COLOR_B, dflt.blue);
//        
//        prefs.setDefault(EditorPreferencePage.LINK_DOCUMENTATION, true);
//        prefs.setDefault(EditorPreferencePage.BRING_DOCUMENTATION_TO_FRONT, false);
//        prefs.setDefault(EditorPreferencePage.SHOW_GENERATED, false);
        
        // logging preferences
        prefs.setDefault(LoggingPreferencePage.LOG_LEVEL, LoggingPreferencePage.DEFAULT_LOG_LEVEL_INDEX);
        prefs.setDefault(LoggingPreferencePage.LOG_DIRECTORY, LoggingPreferencePage.DEFAULT_LOG_DIRECTORY);
        prefs.setDefault(LoggingPreferencePage.BRING_CONSOLE_TO_FRONT, true);
    }
}
