// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.wizards.page;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import silecs.model.exception.SilecsException;
import silecs.utils.SilecsConstants;
import silecs.utils.SilecsUtils;
import silecs.utils.Version;

public class SilecsProjectPage extends WizardPage {
    
    private Text projectNameText;
    private String projectNameDescription;

    private Combo silecsVersionText;
    private String silecsVersionDescription;
    
    private String projectNamePattern;
    private int maxNameLength;
    

    private class PatternMatcherListener implements ModifyListener{
        
        @Override
        public void modifyText(ModifyEvent e) {
            String name = projectNameText.getText();
            setErrorMessage(null);
            setMessage(null, IMessageProvider.WARNING);
            setPageComplete(true);
            
            if (!name.matches(projectNamePattern)) {
                setErrorMessage(SilecsConstants.INVAILD_PROJECT_NAME + "\n" + projectNamePattern);
                setPageComplete(false);
            }   else if(name.length() > maxNameLength) {
                setErrorMessage("Name cannot be longer than " + maxNameLength + " characters");
                setPageComplete(false);
            }   else if (workspaceContainsProject(projectNameText.getText())) {
                setErrorMessage("Project with this name already exists in the workspace");
                setPageComplete(false);
            }
        }
        
    }
    
    /**
     * @param pageName
     * @param projectNameLabelDescryption For example "Enter project name:"
     * @param projectVersionLabelDescryption
     * @param type
     */
    public SilecsProjectPage(String pageName, SilecsConstants.ProjectType type, String projectNameLabelDescryption,
            String silecsVersionLabelDescryption) {
        super("New Silecs Project Page");
        setTitle(pageName);

        projectNameDescription = projectNameLabelDescryption;
        silecsVersionDescription = silecsVersionLabelDescryption;
        
        if (type == SilecsConstants.ProjectType.DEPLOY_PROJECT) {
            projectNamePattern = SilecsConstants.DEPLOY_PATTERN;
            maxNameLength = SilecsConstants.MAX_DEPLOY_LENGTH;
        }
        else if (type == SilecsConstants.ProjectType.DESIGN_PROJECT) {
            projectNamePattern = SilecsConstants.DESIGN_PATTERN;
            maxNameLength = SilecsConstants.MAX_DESIGN_LENGTH;
        }
    }
    

    @Override
    public void createControl(Composite parent) {
        Composite container = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout();
        container.setLayout(layout);
        layout.numColumns = 2;

        Label classNameLabel = new Label(container, SWT.NONE);
        classNameLabel.setText(projectNameDescription);

        projectNameText = new Text(container, SWT.BORDER | SWT.SINGLE);
        projectNameText.setText("");
        projectNameText.addModifyListener(new PatternMatcherListener());
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);
        projectNameText.setLayoutData(gd);

        Label classVersionLabel = new Label(container, SWT.NONE);
        classVersionLabel.setText(silecsVersionDescription);

        silecsVersionText = new Combo(container, SWT.BORDER | SWT.V_SCROLL | SWT.READ_ONLY);
        List<String> availableSilecsVersions = SilecsUtils.readOutAvailableSilecsVersions();
        silecsVersionText.setItems(availableSilecsVersions.toArray(new String[availableSilecsVersions.size()]));
        silecsVersionText.select(SilecsConstants.DEFAULT_SILECS_VERSION_INDEX);
        GridData dataLayout = new GridData();
        dataLayout.horizontalSpan = 2;
        silecsVersionText.setLayoutData(dataLayout);

        // required to avoid an error in the system
        setControl(container);
        setPageComplete(false);
    }

    public String getprojectNameText() {
        return projectNameText.getText();
    }

    public Version getsilecsVersion() throws SilecsException
    {
        return new Version(silecsVersionText.getText());
    }

    public void setprojectNameText(String text) {
        projectNameText.setText(text);
    }

    public void setsilecsVersionText(String text) {
        silecsVersionText.setText(text);
    }

    private boolean workspaceContainsProject(String name) {
        IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
        IProject project = workspaceRoot.getProject(name);
        return project.exists();
    }
}
