// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.editor.internal;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.LabelProviderChangedEvent;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.wst.xml.ui.internal.tabletree.XMLTableTreeContentProvider;
import org.w3c.dom.Node;

import silecs.utils.XmlUtils;


/**
 * Label provider for the design view of the FesaEditor
 */
@SuppressWarnings("restriction")
public class SilecsTableColumnViewerLabelProvider extends CellLabelProvider {

    private static final int TOOLTIP_DELAY_MS = 500;

    // since we could not have multiple inheritance in java,
    // we inject the base label provider in this CellLabelProvider
    // which adds support for tool-tips in the registered treeViewer
    private final XMLTableTreeContentProvider baseLabelProvider;

    // These 2 maps behave like a bidirectional map
    // marker -> node
    // node -> Set<marker>
    private final Map<IMarker, Node> markerToNode = new HashMap<>();
    private final Map<Node, Set<IMarker>> nodeToMarkers = new HashMap<>();

    public SilecsTableColumnViewerLabelProvider(XMLTableTreeContentProvider baseLabelProvider) {
        this.baseLabelProvider = baseLabelProvider;
    }

    /**
     * Register an error/warning with its associated node
     * Fire a label provider changed event
     * 
     * @param marker
     * @param node
     */
    public void addFault(IMarker marker, Node node) {
        markerToNode.put(marker, node);
        if (nodeToMarkers.containsKey(node)) {
            nodeToMarkers.get(node).add(marker);
        } else {
            Set<IMarker> markers = new HashSet<>();
            markers.add(marker);
            nodeToMarkers.put(node, markers);
        }
        
        fireLabelProviderChanged(new LabelProviderChangedEvent(this, node));
    }

    /**
     * Remove the marker and its associated node from the faulty list
     * Fire a label provider changed event
     * 
     * @param marker
     */
    public void removeFault(IMarker marker) {
        Node node = markerToNode.remove(marker);
        if (node != null) {
            Set<IMarker> markers = nodeToMarkers.get(node);
            markers.remove(marker);
            if (markers.isEmpty()) {
                nodeToMarkers.remove(node);
            }
            fireLabelProviderChanged(new LabelProviderChangedEvent(this, node));
        }
    }

    /**
     * Clears all the registered marker/nodes
     */
    public void clearFaults() {
        markerToNode.clear();
        nodeToMarkers.clear();
    }

    /**
     * Provide a tool-tip for the faulty node with the error message of the associated markers
     */
    @Override
    public String getToolTipText(Object element) {
        if (nodeToMarkers.containsKey(element)) {
            Set<IMarker> markers = nodeToMarkers.get(element);
            StringBuilder sb = new StringBuilder();
            for (IMarker marker : markers) {
                try {
                    sb.append(marker.getAttribute(IMarker.MESSAGE) + "\n");
                } catch (CoreException e) {
                    // don't do anything
                }
            }
            
            return XmlUtils.finalize(sb, "\n"); 
        }

        return null;
    }

    @Override
    public Point getToolTipShift(Object object) {
        return new Point(5, 5);
    }

    @Override
    public int getToolTipDisplayDelayTime(Object object) {
        return TOOLTIP_DELAY_MS;
    }

    @Override
    public int getToolTipTimeDisplayed(Object object) {
        return -1;
    }

    /**
     * Use the base label provider for the content If the element is fault, compute the background color
     */
    @Override
    public void update(ViewerCell cell) {
        Object element = cell.getElement();
        int index = cell.getColumnIndex();

        cell.setText(baseLabelProvider.getColumnText(element, index));
        cell.setImage(baseLabelProvider.getColumnImage(element, index));

        cell.setForeground(baseLabelProvider.getForeground(element, index));
        if (nodeToMarkers.containsKey(element)) {
            // background of the faulty cell
            cell.setBackground(computeCellBackground((Node) element));
        } else {
            cell.setBackground(baseLabelProvider.getBackground(element, index));
        }
    }

    /**
     * Helper to compute the background color of a faulty cell
     * 
     * @param node
     * @return
     */
    private Color computeCellBackground(Node node) {
        Set<IMarker> markers = nodeToMarkers.get(node);
        int color = SWT.COLOR_YELLOW;
        for (IMarker marker : markers) {
            if (marker.getAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR) == IMarker.SEVERITY_ERROR) {
                color = SWT.COLOR_RED;
                break;
            }
        }
        return Display.getCurrent().getSystemColor(color);
    }
}
