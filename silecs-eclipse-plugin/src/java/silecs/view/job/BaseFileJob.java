// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.job;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.ISchedulingRule;

import silecs.activator.Activator;

public abstract class BaseFileJob extends WorkspaceJob {
    
    protected final IFile file;
    protected final Map<IResource, Exception> failures;
    
    
    /**
     * @param name
     */
    public BaseFileJob(String name, IFile file) {
        super(name);
        this.file = file;
        this.failures = new HashMap<IResource, Exception>();
        
        // set the job rule
        setRule(createJobRule());
    }

    public Map<IResource, Exception> getFailures(){
        return failures;
    }
    
    protected IStatus computeReturnStatus() {
        if (failures.isEmpty()) {
            return Status.OK_STATUS;
        } else {
            // return INFO status so that eclipse does not open the default error pop up
            // we handle manually the error reporting
            return new Status(IStatus.INFO, Activator.PLUGIN_ID, "");
        }
    }

    
    /**
     * Create the job rule to avoid concurrency on the used resources
     * 
     * @return rule used for the job
     */
    protected ISchedulingRule createJobRule() {
        return null;
    }

    protected final ISchedulingRule workspaceRule() {
        return ResourcesPlugin.getWorkspace().getRoot();
    }
    
    
    
}
