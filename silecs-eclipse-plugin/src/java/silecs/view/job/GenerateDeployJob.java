// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.job;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.SubMonitor;

import silecs.control.core.DeployProjectNature;
import silecs.control.labExtension.LabExtensionProvider;
import silecs.control.validation.Validator;
import silecs.utils.OSExecute;
import silecs.utils.SilecsUtils;
import silecs.utils.Version;
import silecs.view.console.ConsoleHandler;
import silecs.view.preferences.MainPreferencePage;

public class GenerateDeployJob extends BaseProjectJob {

    private static final String JOB_NAME = "Generate SILECS Deploy Job";

    IFile fesaDeployDoc_;
    boolean createdNewDeployDoc_;
    
    public GenerateDeployJob(IProject project, IFile fesaDeployDoc, boolean createdNewDeployDoc) {
        super(JOB_NAME, project);
        fesaDeployDoc_ = fesaDeployDoc;
        createdNewDeployDoc_ = createdNewDeployDoc;
    }

    @Override
    public IStatus runInWorkspace(IProgressMonitor monitor) throws CoreException {

        IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
        String workspacePath = workspaceRoot.getLocation().toFile().getPath();

        monitor.beginTask("Generating Code", 1);
        generateDeploy(workspacePath, SubMonitor.convert(monitor, 1));
        monitor.done();

        return computeReturnStatus();
    }

    private void generateDeploy(String workspacePath, IProgressMonitor monitor )
    {
        String projectName = project.getName();
        
        try {
        	String deployPath = SilecsUtils.getSilecsDeployFilePath(workspacePath,projectName);
	        String deployVersion = SilecsUtils.getDeployVersion(deployPath);
	        Version silecsVersion = SilecsUtils.getSilecsVersionFromProject(project);
	        LabExtensionProvider labSpecificExtension = LabExtensionProvider.getLabExtensionProvider();
	        
	        monitor.beginTask("Generating: " + projectName, 2);
	        
	        ConsoleHandler.printMessage("Generating Code for: " + projectName, false);
	        if (!Validator.isDocumentValid(SilecsUtils.getSilecsDeployFile(project)))
	        {
	        	ConsoleHandler.printError( "Failed to generated code for: '" +  projectName + "'. Please fix all xml errors first!", true);
	        	return;
	        }

	        OSExecute.executePython("genparam","genParam",silecsVersion,new String[]{workspacePath, projectName, deployVersion,silecsVersion.toString()});
	        OSExecute.executePython("genplcsrc","genPlcSrc",silecsVersion,new String[]{workspacePath, projectName,silecsVersion.toString()});
	        OSExecute.executePython("genduwrapper","genDuWrapper",silecsVersion,new String[]{workspacePath, projectName, deployVersion});
	        OSExecute.executePython("generateBuildEnvironment","genCProjectFile",silecsVersion,new String[]{project.getLocation().toString()});

            String silecsLibraryBasePath = MainPreferencePage.getSilecsLibraryBasePath(silecsVersion);
            String snap7LibraryBasePath = MainPreferencePage.getSNAP7LibraryBasePath(silecsVersion);
            OSExecute.executePython("generateBuildEnvironment","genMakefileDU",silecsVersion,new String[]{ workspacePath + "/" + projectName, silecsLibraryBasePath,snap7LibraryBasePath});
            OSExecute.executePython("fillFESADeployUnit","fillDeployUnit",silecsVersion,new String[]{ workspacePath, projectName, labSpecificExtension.getFESADeployUnitSchemaPath(), MainPreferencePage.getFESAVersion() });
            
            DeployProjectNature.addFESADeployUnitNature(project,monitor);
            DeployProjectNature.addBuildSpecs(project);
            
            project.refreshLocal(IResource.DEPTH_INFINITE, null);
            
            ConsoleHandler.printMessage("Succeeded in generated code for " + projectName, true);
            
            if(createdNewDeployDoc_)
            	SilecsUtils.openInEditor(fesaDeployDoc_);
            
            monitor.worked(1);

        } catch (Exception e) {
            e.printStackTrace();
            ConsoleHandler.printError("Failed to generate code for " + projectName, true);
            ConsoleHandler.printStackTrace(e);
        }
    }
}