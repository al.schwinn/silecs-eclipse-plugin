// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.job;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.SubMonitor;

import silecs.control.core.DesignProjectNature;
import silecs.control.labExtension.LabExtensionProvider;
import silecs.control.validation.Validator;
import silecs.utils.OSExecute;
import silecs.utils.SilecsUtils;
import silecs.utils.Version;
import silecs.view.console.ConsoleHandler;
import silecs.view.preferences.MainPreferencePage;

public class GenerateClassJob extends BaseProjectJob {
	
    private static final String JOB_NAME = "Generate SILECS Class Job";

    IFile fesaDesignXML;
    
    public GenerateClassJob(IProject project, IFile fesaDesignDoc) {
        super(JOB_NAME, project);
        fesaDesignXML = fesaDesignDoc;
    }

    @Override
    public IStatus runInWorkspace(IProgressMonitor monitor) throws CoreException {
        
        IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
        String workspacePath = workspaceRoot.getLocation().toFile().getPath();

        monitor.beginTask("Generate Fesa Classes", 1);
        
        generateFesaClass(workspacePath, SubMonitor.convert(monitor, 1));
        DesignProjectNature.addFESAClassNature(project,monitor);
        DesignProjectNature.addBuildSpecs(project);
        monitor.done();
        
        return computeReturnStatus();
    }

    boolean generateFesaClass(String workspacePath, IProgressMonitor monitor)
    {
    	String projectName = project.getName();
    	String projectPath = workspacePath + "/" + projectName;
        try {
        	String silecsDesignFilePath = SilecsUtils.getSilecsDesignFilePath(workspacePath, projectName);
	        Version silecsVersion = SilecsUtils.getSilecsVersionFromProject(project);
	        String silecsLibraryBasePath = MainPreferencePage.getSilecsLibraryBasePath(silecsVersion);
	        LabExtensionProvider labSpecificExtension = LabExtensionProvider.getLabExtensionProvider();
	        ConsoleHandler.printMessage("Generating FESAaa class: " + projectName, false);
	        
	        if (!Validator.isDocumentValid(SilecsUtils.getSilecsDesignFile(project)))
	        {
	        	ConsoleHandler.printError( "Failed to generated FESA class: '" +  projectName + "'. Please fix all design errors first!", true);
	            return false;
	        }
	        
            monitor.beginTask("Generating " + projectName, 2);
            OSExecute.executePython("generateFesaDesign","fillDesignFile",silecsVersion,new String[]{MainPreferencePage.getFESAVersion(),projectName,workspacePath,labSpecificExtension.getFESADesignSchemaPath()});
            OSExecute.executePython("generateSourceCode","genCppFiles",silecsVersion,new String[]{projectName, workspacePath,silecsDesignFilePath}); // attention - Cpp Codegen has to happen AFTER design file codegen
            OSExecute.executePython("generateBuildEnvironment","genMakefileClass",silecsVersion,new String[]{projectPath,silecsLibraryBasePath});
            OSExecute.executePython("generateBuildEnvironment","genCProjectFile",silecsVersion,new String[]{projectPath.toString()});
            project.refreshLocal(IResource.DEPTH_INFINITE, null);
            SilecsUtils.openInEditor(fesaDesignXML);
            monitor.worked(1);
        } catch (Exception e){
            e.printStackTrace();
            ConsoleHandler.printError("Failed to generate FESA class: '" + projectName + "' Reason: " + e.getMessage(), true);
            ConsoleHandler.printStackTrace(e);
            return false;
        }
        
        monitor.done();
        ConsoleHandler.printMessage( "Successfully generated FESA class: " + projectName, true);
        return true;

    }
}
