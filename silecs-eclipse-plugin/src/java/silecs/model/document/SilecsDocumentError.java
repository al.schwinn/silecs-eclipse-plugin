// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.model.document;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.w3c.dom.Node;
import org.xml.sax.SAXParseException;

import silecs.utils.XmlUtils;


public class SilecsDocumentError {

    public enum ErrorLevel {
        INFO,
        WARNING,
        ERROR,
        FATAL
    }

    /**
     * error level
     */
    public final ErrorLevel level;

    /**
     * text with the description of the error
     */
    public final String message;

    /**
     * line of the error
     */
    public final int lineNumber;

    /**
     * column of the error
     */
    public final int columnNumber;

    /**
     * Element which cause the error
     */
    public final String xPath;
    
    /**
     * If an identity constraint has been violated, this contains the name of the violated key
     */
    public final String violatedKey;
    
    /**
     * If an identity constraint has been violated, this contains the incorrect value which is the cause of the error
     */
    public final String incorrectValue;
    
    private static final String CONSTRAINT_MESSAGE_PATTERN = "cvc-identity-constraint.4.3: Key '(.*)' with value '(.*)' not found for identity constraint of element '(.*)'\\.";

    public SilecsDocumentError(ErrorLevel level, String message, int lineNumber, int columnNumber, String xPath) {
        this.level = level;
        this.message = message;
        this.lineNumber = lineNumber;
        this.columnNumber = columnNumber;
        this.xPath = xPath;
        
        String key = "";
        String value = "";
        Pattern constraintPattern = Pattern.compile(CONSTRAINT_MESSAGE_PATTERN);
        Matcher m = constraintPattern.matcher(message);
        if (m.find()) {
            key = m.group(1);
            value =  m.group(2);
        }
        
        this.violatedKey = key;
        this.incorrectValue = value;
    }

    public SilecsDocumentError(ErrorLevel level, SAXParseException e, Node node) {
        this(level, e.getMessage(), e.getLineNumber(), e.getColumnNumber(), XmlUtils.xPathFromNode(node));
    }
    
    public boolean isIdentityConstraintError() {
        return !violatedKey.isEmpty();
    }

    @Override
    public String toString() {
        return lineNumber + ":" + columnNumber + " => " + message;
    }
}

