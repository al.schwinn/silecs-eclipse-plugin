// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.control.validation.internal;

import java.util.ArrayList;
import java.util.List;

import org.apache.xerces.parsers.DOMParser;
import org.eclipse.core.resources.IFile;
import org.w3c.dom.Element;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.SAXParseException;

import silecs.model.document.SilecsDocumentError;
import silecs.model.document.SilecsDocumentError.ErrorLevel;
import silecs.view.console.ConsoleHandler;

public class Handler implements ErrorHandler
{
    private static final String CURRENT_NODE = "http://apache.org/xml/properties/dom/current-element-node";

    private final DOMParser parser;
    private final String fileName;
    private List<SilecsDocumentError> errors;

    public Handler(IFile xmlFile, DOMParser parser) {
        this.parser = parser;
        this.fileName = xmlFile.getName();
        this.errors = new ArrayList<SilecsDocumentError>();
    }

    @Override
    public void warning(SAXParseException e) throws SAXException {
        addError(ErrorLevel.WARNING, e);
    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        addError(ErrorLevel.ERROR, e);
    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        addError(ErrorLevel.FATAL, e);
    }

    public final List<SilecsDocumentError> getErrors() {
        return errors;
    }

    private void addError(ErrorLevel level, SAXParseException e) {
        Element element = null;
        try
        {
            element = (Element) parser.getProperty(CURRENT_NODE);
        }
        catch (SAXNotRecognizedException | SAXNotSupportedException e1)
        {
        	ConsoleHandler.printStackTrace(e1);
        	return;
        }
        SilecsDocumentError error = null;
        String file = e.getSystemId();
        if (!file.endsWith(fileName)) {
            // if the error is in the included xml or in the schema, we want to report the problem
            // on the validated file but with a specific message
            error = new SilecsDocumentError(level, "Error in included file: " + file + " at " + e.getLineNumber() + ":"
                    + e.getColumnNumber() + " => " + e.getMessage(), -1, -1, null);
        } else {
            error = new SilecsDocumentError(level, e, element);
        }

        errors.add(error);
    }
}
