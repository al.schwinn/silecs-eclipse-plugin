// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.control.validation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.resources.IFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import silecs.model.document.SilecsDocumentError;
import silecs.model.exception.SilecsException;
import silecs.utils.SilecsUtils;
import silecs.utils.Version;

public class internalRules {

    public static List<SilecsDocumentError> validate(IFile file) throws ParserConfigurationException, SAXException, IOException, SilecsException
    {
    	// Some of the XSD-checks are version-specific. This is painful and will vanish some day when using XSD1.1 for all checks
    			
    	List<SilecsDocumentError> errors = new ArrayList<SilecsDocumentError>();

    	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    	File baseFile = new File(file.getLocationURI());

    	Version documentSilecsVersion = SilecsUtils.getSilecsVersionFromFile(file);
    	
    	if( SilecsUtils.isSilecsDesign(file) )
    	{
    		if( documentSilecsVersion.isOlderThan(new Version("2.0.0")) )
    		{
    		    Document doc = dBuilder.parse(baseFile);
        		errors.addAll(classRule_RegisterDim2(doc));
        		errors.addAll(classRule_StringLength(doc));
    			errors.addAll(classRule_RO_Slave(doc));
    			errors.addAll(classRule_WO_Master(doc));
            }

    	}
    	if( SilecsUtils.isSilecsDeploy(file) )
    	{
    	    Document doc = dBuilder.parse(baseFile);
    		errors.addAll(deployRule_addressing_SchneiderM340(doc));
    		errors.addAll(deployRule_addressing_BeckhoffCX90xx(doc));
    	}
        return errors;
    }
    
    private static String getXPath(Node node)
    {
        Node parent = node.getParentNode();
        if (parent == null)
        {
            return "/";
        }
        
        if(!((Element)node).getAttribute("name").isEmpty())
        	return getXPath(parent) + "/" + node.getNodeName() + "[@name='"+ ((Element)node).getAttribute("name") + "']";
        
        return getXPath(parent) + "/" + node.getNodeName();
    }
    
    private static List<SilecsDocumentError> classRule_RegisterDim2(Document doc)
    {
    	List<SilecsDocumentError> errors = new ArrayList<SilecsDocumentError>();
    	NodeList regNodes = doc.getElementsByTagName("Register");
    	for( int i = 0; i< regNodes.getLength() ; i++)
    	{
    		Element register = (Element)regNodes.item(i);
    		if( register.hasAttribute("array-dim2") && !register.hasAttribute("array-dim1") )
    		{
                SilecsDocumentError newError = new SilecsDocumentError( SilecsDocumentError.ErrorLevel.ERROR, "array-dim1 is required if array-dim2 is defined", -1, -1,getXPath(register));
                errors.add(newError);
    		}
    	}
    	return errors;
    }
    
    private static List<SilecsDocumentError> classRule_RO_Slave(Document doc)
    {
    	List<SilecsDocumentError> errors = new ArrayList<SilecsDocumentError>();
    	NodeList blockNodes = doc.getElementsByTagName("Block");
    	for( int i = 0; i< blockNodes.getLength() ; i++)
    	{
    		Element block = (Element)blockNodes.item(i);
    		if( block.getAttribute("mode").equals("READ-ONLY") )
    		{
    			NodeList registerNodes = block.getElementsByTagName("Register");
    			for( int j = 0; j< registerNodes.getLength() ; j++)
    			{
    				Element register = (Element)registerNodes.item(j);
    	    		if( register.getAttribute("synchro").equals("SLAVE") )
    	    		{
    	                SilecsDocumentError newError = new SilecsDocumentError( SilecsDocumentError.ErrorLevel.ERROR, "Register cannot have SLAVE synchro attribute within READ-ONLY block", -1, -1,getXPath(register));
    	                errors.add(newError);
    	    		}
    			}
    		}
    	}
    	return errors;
    }
    
    private static List<SilecsDocumentError> classRule_WO_Master(Document doc)
    {
    	List<SilecsDocumentError> errors = new ArrayList<SilecsDocumentError>();
    	NodeList blockNodes = doc.getElementsByTagName("Block");
    	for( int i = 0; i< blockNodes.getLength() ; i++)
    	{
    		Element block = (Element)blockNodes.item(i);
    		if( block.getAttribute("mode").equals("WRITE-ONLY"))
    		{
    			NodeList registerNodes = block.getElementsByTagName("Register");
    			for( int j = 0; j< registerNodes.getLength() ; j++)
    			{
    				Element register = (Element)registerNodes.item(j);
    	    		if( register.getAttribute("synchro").equals("MASTER") )
    	    		{
    	                SilecsDocumentError newError = new SilecsDocumentError( SilecsDocumentError.ErrorLevel.ERROR, "Register cannot have MASTER synchro attribute within WRITE-ONLY block", -1, -1,getXPath(register));
    	                errors.add(newError);
    	    		}
    			}
    		}
    	}
    	return errors;
    }
    	
    private static List<SilecsDocumentError> classRule_StringLength(Document doc)
    {
    	List<SilecsDocumentError> errors = new ArrayList<SilecsDocumentError>();
		NodeList registerNodes = doc.getElementsByTagName("Register");
		for( int j = 0; j< registerNodes.getLength() ; j++)
		{
			Element register = (Element)registerNodes.item(j);
    		if( register.getAttribute("format").equals("string") == false )
    		{
    			if( register.hasAttribute("string-len"))
    			{
	                SilecsDocumentError newError = new SilecsDocumentError( SilecsDocumentError.ErrorLevel.ERROR, "string-len attribute only allowed for @format='string'.", -1, -1,getXPath(register));
	                errors.add(newError);
    			}
    		}
		}
    	return errors;
    }

    private static List<SilecsDocumentError> deployRule_addressing_SchneiderM340(Document doc)
    {
    	List<SilecsDocumentError> errors = new ArrayList<SilecsDocumentError>();
		NodeList schneiderPLCNode = doc.getElementsByTagName("Schneider-PLC");
		for( int i = 0; i< schneiderPLCNode.getLength() ; i++)
		{
			Element plc = (Element)schneiderPLCNode.item(i);
    		if( plc.getAttribute("model").equals("M340"))
    		{
    			int basAddress = Integer.parseInt( plc.getAttribute("base-address"));
    			if( basAddress%2 != 0 )
    			{
	                SilecsDocumentError newError = new SilecsDocumentError( SilecsDocumentError.ErrorLevel.ERROR, "Only even addressing is allowed for UNITY_M340. Please change the base address value to an even number.", -1, -1,getXPath(plc));
	                errors.add(newError);
    			}
    		}
		}
    	return errors;
    }
    
    private static List<SilecsDocumentError> deployRule_addressing_BeckhoffCX90xx(Document doc)
    {
    	List<SilecsDocumentError> errors = new ArrayList<SilecsDocumentError>();
		NodeList schneiderPLCNode = doc.getElementsByTagName("Beckhoff-PLC");
		for( int i = 0; i< schneiderPLCNode.getLength() ; i++)
		{
			Element plc = (Element)schneiderPLCNode.item(i);
    		if( plc.getAttribute("model").equals("CX9020"))
    		{
    			int basAddress = Integer.parseInt( plc.getAttribute("base-address"));
    			if( basAddress%2 != 0 )
    			{
	                SilecsDocumentError newError = new SilecsDocumentError( SilecsDocumentError.ErrorLevel.ERROR, "Only even addressing is allowed for TWINCAT_CX9020. Please change the base address value to an even number.", -1, -1,getXPath(plc));
	                errors.add(newError);
    			}
    		}
		}
    	return errors;
    }
    
}
