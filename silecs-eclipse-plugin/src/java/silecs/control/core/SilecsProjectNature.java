// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.control.core;


import org.eclipse.cdt.core.CCProjectNature;
import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;

import org.eclipse.core.runtime.SubMonitor;

import silecs.model.exception.SilecsException;
import silecs.utils.SilecsConstants;
import silecs.view.console.ConsoleHandler;


public abstract class SilecsProjectNature extends CCProjectNature {

    public static final String PROJECT_FILE = ".project";
    public static final String CDT_PROJECT_FILE = ".cproject";

    private static final String ORG_ECLIPSE_CDT_MANAGEDBUILDER_CORE_SCANNER_CONFIG_NATURE = "org.eclipse.cdt.managedbuilder.core.ScannerConfigNature";
	private static final String ORG_ECLIPSE_CDT_MANAGEDBUILDER_CORE_MANAGED_BUILD_NATURE = "org.eclipse.cdt.managedbuilder.core.managedBuildNature";
	
	private static final String FESA_BUILD_COMMAND_1 = "org.eclipse.cdt.managedbuilder.core.genmakebuilder";
	private static final String FESA_BUILD_COMMAND_2 = "org.eclipse.cdt.managedbuilder.core.ScannerConfigBuilder";
	
    protected static void removeAllNatures(IProject project, IProgressMonitor mon) throws CoreException {
        IProgressMonitor monitor = mon;
        if (mon == null) {
            monitor = new NullProgressMonitor();
        }
        try {
            IProjectDescription desc = project.getDescription();
            desc.setNatureIds(new String[0]);
            project.setDescription(desc, null);
        } finally {
            monitor.done();
        }
    }
    
    protected static void addCppNature(IProject project, IProgressMonitor mon) throws CoreException {
        IProgressMonitor monitor = mon;
        if (mon == null) {
            monitor = new NullProgressMonitor();
        }
        monitor.beginTask("Adding C++ Nature to project", 2);
        
        try {
            addNature(project, C_NATURE_ID, SubMonitor.convert(monitor, 1));
            addNature(project, CC_NATURE_ID, SubMonitor.convert(monitor, 1));
        } finally {
            monitor.done();
        }
    }
    
    protected static void removeCppNature(IProject project, IProgressMonitor mon) throws CoreException {
        IProgressMonitor monitor = mon;
        if (mon == null) {
            monitor = new NullProgressMonitor();
        }
        
        monitor.beginTask("Removing C++ Nature from project", 2);
        
        try {
            removeNature(project, C_NATURE_ID, SubMonitor.convert(monitor, 1));
            removeNature(project, CC_NATURE_ID, SubMonitor.convert(monitor, 1));
        } finally {
            monitor.done();
        }
    }

    @Override
    public void configure() throws CoreException {
        // Do nothing. this allow to put fesa nature first
    }
    
    public static SilecsConstants.ProjectType getProjectType(IProject project) throws CoreException, SilecsException {
        if (project.hasNature(DesignProjectNature.NATURE_ID)) {
            return SilecsConstants.ProjectType.DESIGN_PROJECT;
        } else if (project.hasNature(DeployProjectNature.NATURE_ID)) {
            return SilecsConstants.ProjectType.DEPLOY_PROJECT;
        } else {
            throw new SilecsException("Project " + project.getName() + " has no fesa nature");
        }
    }
    
    protected static void addManageBuilderNature(IProject project, IProgressMonitor mon) throws CoreException {
        IProgressMonitor monitor = mon;
        if (mon == null) {
            monitor = new NullProgressMonitor();
        }
        monitor.beginTask("Adding Managebuilder Nature to project", 2);
        
        try {
        	addCppNature(project,mon);
            addNature(project, ORG_ECLIPSE_CDT_MANAGEDBUILDER_CORE_MANAGED_BUILD_NATURE, SubMonitor.convert(monitor, 1));
            addNature(project, ORG_ECLIPSE_CDT_MANAGEDBUILDER_CORE_SCANNER_CONFIG_NATURE, SubMonitor.convert(monitor, 1));
        } finally {
            monitor.done();
        }
    }
    
    protected static void removeManageBuilderNature(IProject project, IProgressMonitor mon) throws CoreException {
        IProgressMonitor monitor = mon;
        if (mon == null) {
            monitor = new NullProgressMonitor();
        }
        
        monitor.beginTask("Removing Managebuilder Nature from project", 2);
        
        try {
        	removeCppNature(project,mon);
            removeNature(project, ORG_ECLIPSE_CDT_MANAGEDBUILDER_CORE_MANAGED_BUILD_NATURE, SubMonitor.convert(monitor, 1));
            removeNature(project, ORG_ECLIPSE_CDT_MANAGEDBUILDER_CORE_SCANNER_CONFIG_NATURE, SubMonitor.convert(monitor, 1));
        } finally {
            monitor.done();
        }
    }
    
    public static void addBuildSpecs(IProject project)
    {
        try {
        	IProjectDescription desc = project.getDescription();
        	ICommand[] commands = new ICommand[2];
            final ICommand command0 = desc.newCommand();
            final ICommand command1 = desc.newCommand();
            command0.setBuilderName(FESA_BUILD_COMMAND_1);
            command1.setBuilderName(FESA_BUILD_COMMAND_2);
            commands[0] = command0;
            commands[1] = command1;
            
            //remove auto-build options
            commands[0].setBuilding(IncrementalProjectBuilder.AUTO_BUILD, false);
            commands[1].setBuilding(IncrementalProjectBuilder.AUTO_BUILD, false);
            commands[1].setBuilding(IncrementalProjectBuilder.CLEAN_BUILD, false);
        	desc.setBuildSpec(commands);
            project.setDescription(desc, null);
        } catch (CoreException e) {
            e.printStackTrace();
            ConsoleHandler.printStackTrace(e);
        }
    }
}
