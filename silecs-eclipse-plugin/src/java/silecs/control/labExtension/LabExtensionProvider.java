package silecs.control.labExtension;

import java.io.File;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;

import silecs.model.exception.SilecsException;
import silecs.utils.Version;

public interface LabExtensionProvider
{

    // virtual methods to be implemented in lab-part

    String getSilecsWiki();

    void installSilecsParam(File paramFile);

    String getDefaultSilecsBasePath();

    String getDefaultFesaBasePath();

    File[] getFesaDesignTemplates() throws SilecsException;

    File[] getFesaDeployUnitTemplates() throws SilecsException;

    String getFESADeployUnitSchemaPath() throws SilecsException;

    String getFESADesignSchemaPath() throws SilecsException;

    List<Version> readOutAvailableFesaVersions() throws SilecsException;

    public static LabExtensionProvider getLabExtensionProvider() throws SilecsException
    {
        try
        {
            IConfigurationElement[] configElements = Platform.getExtensionRegistry().getConfigurationElementsFor("silecs.control.labExtension.LabExtensionProvider");

            if (configElements.length == 0)
                throw new SilecsException("No lab-specific extension found for interface LabExtensionProvider");

            if (configElements.length > 1)
                throw new SilecsException("There should be exactly one Extension for the LabExtensionProvider");

            return (LabExtensionProvider) configElements[0].createExecutableExtension("class");
        }
        catch (Exception ex)
        {
            throw new SilecsException("Exception while getting LabExtensionProvider: " + ex.getMessage());
        }
    }

}
