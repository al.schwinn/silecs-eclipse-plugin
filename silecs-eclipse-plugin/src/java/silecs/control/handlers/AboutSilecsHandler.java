// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.control.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import silecs.control.labExtension.LabExtensionProvider;
import silecs.model.exception.SilecsException;
import silecs.view.console.ConsoleHandler;
import silecs.view.dialogs.SilecsMessageDialog;

/**
 * This handler displays basic information about the Silecs tool.
 */
public class AboutSilecsHandler extends AbstractHandler implements IHandler {

    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {
        Shell shell = PlatformUI.getWorkbench().getDisplay().getActiveShell();

        LabExtensionProvider labSpecificExtension;
        try
        {
            labSpecificExtension = LabExtensionProvider.getLabExtensionProvider();
        }
        catch (SilecsException e)
        {
            ConsoleHandler.printStackTrace(e);
            return null;
        }
        
        String dialogMessage = "About SILECS\n\n"
                + "A 'Design project' defines the data to be exchanged with the controller (blocks and the registers) in a hardware independent manners while a 'Deploy project' binds several design instances on a physical controller. \n"
                + "Purpose of this tool is to provide an easy-to-use graphical interface to guide the user in the definition of his design/deploy projects and to generate the client and controller resources necessary for the communication via the Silecs communication library.\n\n"
                
                + "If you are using this tool you may also be interests in: \n"
                + "\u2022   Silecs communication library \n"
                + "\u2022   Silecs diagnostic tool \n\n"
                + "Support contact: silecs-support@gsi.de \n"
                + "Silecs wiki:     " + labSpecificExtension.getSilecsWiki() + "\n\n\n"
                
                + "Copyright 2016 CERN and GSI\n"
                + "This program is free software: you can redistribute it and/or modify\n"
                + "it under the terms of the GNU General Public License as published by\n"
                + "the Free Software Foundation, either version 3 of the License, or\n"
                + "(at your option) any later version.\n\n"

                + "This program is distributed in the hope that it will be useful,\n"
                + "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
                + "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
                + "GNU General Public License for more details.\n\n"
                + "You should have received a copy of the GNU General Public License\n"
                + "along with this program.  If not, see <http://www.gnu.org/licenses/>.\n\n";


        MessageDialog dialog = new SilecsMessageDialog(shell, "About Silecs", null, dialogMessage,
                MessageDialog.INFORMATION, new String[] { "OK" }, 0);
        dialog.open();
        return null;
    }
}
