// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.control.handlers;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

import silecs.activator.Activator;
import silecs.control.core.DeployProjectNature;
import silecs.control.core.DesignProjectNature;
import silecs.control.labExtension.LabExtensionProvider;
import silecs.model.exception.SilecsException;
import silecs.utils.SilecsUtils;
import silecs.view.console.ConsoleHandler;
import silecs.view.job.BaseProjectJob;
import silecs.view.job.GenerateClassJob;
import silecs.view.job.GenerateDeployJob;
import silecs.view.preferences.MainPreferencePage;
import silecs.view.wizards.GenerateCodeWizard;

public class GenerateCodeHandler extends BaseHandler implements IHandler {
	
	@Override
    public Object execute(ExecutionEvent event) throws ExecutionException {

		ConsoleHandler.clear();
		
        BaseProjectJob job = null;
        try {
            IFile silecsFile = SilecsUtils.extractSilecsFileFromEvent(event);
            IProject project = silecsFile.getProject();
            SilecsUtils.checkSilecsVersion(project);
            
        	IFolder src = project.getFolder("src");
			if(project.hasNature(DesignProjectNature.NATURE_ID) )
			{
	    		IFile fesaDesignDoc = src.getFile(project.getName() + ".design");
	    		boolean designDocExists = fesaDesignDoc.exists();
	    		if(designDocExists)
	    		{
	    			IFile backup = src.getFile(fesaDesignDoc.getName() + ".backup");
	    			ConsoleHandler.printMessage("Existing FESA Design document detected - new data will be inserted", true);
	    			ConsoleHandler.printWarning("Please note that old, obsolete xml-elements are not deleted automatically! Sometimes this can lead to an invalid FESA Design document!", true);
	    			ConsoleHandler.printWarning("If this is the case, please remove the obsolete elements by hand.", true);
	    			ConsoleHandler.printMessage("A backup of your old document will be saved at: " + backup.getRawLocation().makeAbsolute(), true);
	    			if(backup.exists())
	    				backup.delete(true, null);
	    			fesaDesignDoc.copy(backup.getFullPath(), true, null);
	    		}
	    		else
	    		{
	    		    LabExtensionProvider labExtensionProvider = LabExtensionProvider.getLabExtensionProvider();
	        		File template = pickTemplate(event,labExtensionProvider.getFesaDesignTemplates());
	        		if( template == null) // cancel pressed
	        			return null;
	        		ConsoleHandler.printMessage("The following template will be used:" + template.getAbsolutePath(), true);
	    		    InputStream source = new FileInputStream(template);
	    		    fesaDesignDoc.create(source, IResource.NONE, null);
	    		}
				job = new GenerateClassJob(project, fesaDesignDoc);
			}
			else if(project.hasNature(DeployProjectNature.NATURE_ID) )
			{
	    		IFile fesaDeployDoc = src.getFile(project.getName() + ".deploy");
	    		boolean deployDocExists = fesaDeployDoc.exists();
	    		if(deployDocExists)
	    		{
	    			IFile backup = src.getFile(fesaDeployDoc.getName() + ".backup");
	    			ConsoleHandler.printMessage("Existing FESA Deploy-Unit document detected - new data will be inserted", true);
	    			ConsoleHandler.printWarning("Please note that old, obsolete xml-elements are not deleted automatically! Sometimes this can lead to an invalid FESA Deploy-Unit document !", true);
	    			ConsoleHandler.printWarning("If this is the case, please remove the obsolete elements by hand.", true);
	    			ConsoleHandler.printMessage("A backup of your old document will be saved at: " + backup.getRawLocation().makeAbsolute(), true);
	    			if(backup.exists())
	    				backup.delete(true, null);
	    			fesaDeployDoc.copy(backup.getFullPath(), true, null);
	    		}
	    		else
	    		{
	    		    LabExtensionProvider labSpecificExtension = LabExtensionProvider.getLabExtensionProvider();
	        		File template = pickTemplate(event,labSpecificExtension.getFesaDeployUnitTemplates());
	        		if( template == null) // cancel pressed
	        			return null;
	        		ConsoleHandler.printMessage("The following template will be used:" + template.getAbsolutePath(), true);
	    		    InputStream source = new FileInputStream(template);
	    		    fesaDeployDoc.create(source, IResource.NONE, null);
	    		}
	    		
				job = new GenerateDeployJob(project, fesaDeployDoc,!deployDocExists);
			}
			else
			{
				ConsoleHandler.printError("Failed to find SILECS Project Nature", true);
				return null;
			}
		} catch (Exception e)
        {
			ConsoleHandler.printError("Failed to generate code: ", true);
			ConsoleHandler.printStackTrace(e);
	        return null;
		}
            	
        job.setUser(true);
        job.schedule();
        
        return null;
    }

	private File pickTemplate(ExecutionEvent event, File[] templates) throws SilecsException
	{
		if(templates.length < 1)
		{
			String error = "Failed to generate FESA Class. No templates found. please fix your FESA-PATH in the preferences!";
			ConsoleHandler.printError(error, true);
			throw new SilecsException(error);
		}
		if( templates.length == 1 )
		{
			return templates[0];
		}
		//TODO: Would be nice to add the possibility to pick a foreign template from the file system
		Shell activeShell = HandlerUtil.getActiveShell(event);
		IWizard wizard = new GenerateCodeWizard(templates);
		WizardDialog dialog = new WizardDialog(activeShell, wizard);
		dialog.open();
		return ((GenerateCodeWizard)wizard).getChoice();
	}
    

}
