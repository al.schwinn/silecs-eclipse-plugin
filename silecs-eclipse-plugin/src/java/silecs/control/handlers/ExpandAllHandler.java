// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.control.handlers;

import java.util.ArrayList;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.PlatformUI;
import silecs.view.explorer.SilecsProjectExplorer;

/**
 * This handler is responsible for expanding selected projects in the silecs explorer.<br>
 * It calls SilecsProjectExplorer's expandHelper to expand projects to src-file level. 
 */
public class ExpandAllHandler extends AbstractHandler implements IHandler {

    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {
        IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
        ArrayList<IProject> projects = new ArrayList<>();
        
        ISelectionService selectionService = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService();

        IStructuredSelection selections = (IStructuredSelection) selectionService
                .getSelection(SilecsProjectExplorer.VIEW_ID);
           
        for(Object selection : selections.toArray()) {
            if(selection instanceof IProject)
                projects.add((IProject)selection);
        }
        
        SilecsProjectExplorer explorer = (SilecsProjectExplorer) PlatformUI.getWorkbench().getActiveWorkbenchWindow()
                .getActivePage().findView(SilecsProjectExplorer.VIEW_ID);

        explorer.expandHelper(root, projects);

        return null;
    }
}
