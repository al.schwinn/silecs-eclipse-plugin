// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.control.handlers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.ide.IDE;

import silecs.control.core.DeployProjectNature;
import silecs.control.core.DesignProjectNature;
import silecs.utils.SilecsUtils;

public abstract class BaseHandler extends AbstractHandler {
    
    
    
    /**
     * Gives the list of selected documents
     * Asks the user to save the file if dirty in an editor
     * 
     * @param event
     * @return
     */
    protected List<IFile> retrieveIeplcFiles(ExecutionEvent event) {
        List<IFile> files = retrieveIeplcFilesNonCheck(event);
        return ensureFileSaved(files);
    }
    
    /**
     * Gives the Ieplc file form file selection or project selection
     * @param event
     * @return
     */
    private List<IFile> retrieveIeplcFilesNonCheck(ExecutionEvent event) {
        List<IFile> files = new ArrayList<IFile>();

        // get from where the command was fired
        Object trigger = event.getTrigger();
        if (trigger instanceof Event) {
            Event ev = (Event) trigger;

            if (ev.widget instanceof MenuItem) {
                // if fired by a popup
                IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getActiveMenuSelection(event);

                for (Object o : selection.toList()) {
                    if (o instanceof IFile) {
                        files.add((IFile) o);
                    } else if (o instanceof IProject) {
                        // if project selected return the design or du file
                        IProject project = (IProject) o;
                        try {
                            files.add(retriveIeplcFileFromProject(project));
                        } catch (CoreException e) {
                            // if project nature can't be retrieve, log but do nothing
                            //LOGGER.error("Could not retrieve project nature for {}", project.getName());
                        } catch (Exception e) {
                            //LOGGER.error("Project nature not supported for {}", project.getName());
                        }

                    }
                }

            } else {
                // the command was fired by a shortcut or a toolbar button
                IEditorPart editor = HandlerUtil.getActiveEditor(event);
                IFileEditorInput input = (IFileEditorInput) editor.getEditorInput();
                files.add(input.getFile());
            }
        }
        
        return files;
    }

    /**
     * Gives the list of selected project
     * 
     * @param event
     * @return
     */
    protected List<IProject> retrieveProjects(ExecutionEvent event) {
        List<IProject> projects = new ArrayList<IProject>();

        // get from where the command was fired
        Object trigger = event.getTrigger();
        if (trigger instanceof Event) {
            Event ev = (Event) trigger;

            if (ev.widget instanceof MenuItem) {
                IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getActiveMenuSelection(event);
                List<?> resources = IDE.computeSelectedResources(selection);
                for (Object o : resources) {
                    if (o instanceof IProject) {
                        projects.add((IProject) o);
                    } else if (o instanceof IFile) {
                        IProject project = ((IFile) o).getProject();

                        if (project != null) {
                            projects.add(project);
                        }
                    }
                }
            } else {
                IEditorPart editor = HandlerUtil.getActiveEditor(event);
                if (editor == null) return projects;
                IFileEditorInput input = (IFileEditorInput) editor.getEditorInput();
                projects.add(input.getFile().getProject());
            }
        }

        if (projects.isEmpty()) {
            //LOGGER.info("No projects could be retrieved");
        }

        return projects;
    }

    /**
     * Finds the Ieplc file from the project root
     * @param project
     * @return
     * @throws Exception 
     * @throws Exception
     */
    private IFile retriveIeplcFileFromProject(IProject project) throws Exception {
        if (project.hasNature(DeployProjectNature.NATURE_ID)) {
            return SilecsUtils.getSilecsDeployFile(project);
        } else if (project.hasNature(DesignProjectNature.NATURE_ID)) {
            return SilecsUtils.getSilecsDesignFile(project);
        } else {
            throw new Exception();
        }
    }
    
    /**
     * Ensure the documents are not dirty in an open editor
     * @param files
     * @return
     */
    private List<IFile> ensureFileSaved(List<IFile> files) {
        if (IDE.saveAllEditors(files.toArray(new IResource[files.size()]), true)) {
            return files;
        } else {
            return Collections.<IFile> emptyList();
        }
    }

}
