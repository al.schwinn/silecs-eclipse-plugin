// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.control.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IFile;

import silecs.view.console.ConsoleHandler;
import silecs.view.job.ValidationJob;
import silecs.utils.SilecsUtils;


public class ValidateHandler extends BaseHandler implements IHandler {
	
    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {

    	ConsoleHandler.clear();
        ConsoleHandler.printMessage("Validating XML  ...", true);
        ValidationJob job;
		try
		{
			IFile file = SilecsUtils.extractSilecsFileFromEvent(event);
			SilecsUtils.checkSilecsVersion(file.getProject());
			job = new ValidationJob(file);
		}
		catch (Exception e)
		{
			 ConsoleHandler.printError("Failed to validate file:", true);
			 ConsoleHandler.printStackTrace(e);
			 return null;
		}

        job.schedule();
        return null;
    }
}
