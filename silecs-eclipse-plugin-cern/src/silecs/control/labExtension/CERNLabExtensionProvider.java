package silecs.control.labExtension;

import java.io.File;
import java.util.List;

import silecs.model.exception.SilecsException;
import silecs.utils.Version;
import silecs.control.fesa.FesaLocationProvider;
import silecs.control.labExtension.LabExtensionProvider;

public class CERNLabExtensionProvider implements LabExtensionProvider
{
    public static final String DEFAULT_SILECS_BASE_PATH = "/common/usr/cscofe/silecs";
    public static final String SILECS_WIKI_URL          = "https://wikis.cern.ch/display/SIL/SILECS+Home";

    public CERNLabExtensionProvider()
    {
    }

    @Override
    public String getSilecsWiki()
    {
        return SILECS_WIKI_URL;
    }

    @Override
    public String getDefaultSilecsBasePath()
    {
        return DEFAULT_SILECS_BASE_PATH;
    }

    @Override
    public String getDefaultFesaBasePath()
    {
        return FesaLocationProvider.DEFAULT_FESA_BASE_PATH;
    }

    @Override
    public void installSilecsParam(File paramFile)
    {
        // Not available yet
    }

    @Override
    public File[] getFesaDesignTemplates() throws SilecsException
    {
        return FesaLocationProvider.getFesaDesignTemplates();
    }

    @Override
    public File[] getFesaDeployUnitTemplates() throws SilecsException
    {
        return FesaLocationProvider.getFesaDeployUnitTemplates();
    }

    @Override
    public String getFESADeployUnitSchemaPath() throws SilecsException
    {
        return FesaLocationProvider.getFESADeployUnitSchemaPath();
    }

    @Override
    public String getFESADesignSchemaPath() throws SilecsException
    {
        return FesaLocationProvider.getFESADesignSchemaPath();
    }

    @Override
    public List<Version> readOutAvailableFesaVersions() throws SilecsException
    {
        return FesaLocationProvider.readOutAvailableFesaVersions();
    }

}
