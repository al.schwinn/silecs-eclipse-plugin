#! /bin/bash

kinit

function copyFileToWebDav
{

cadaver -t <<EOF
open https://www.acc.gsi.de/dav/eclipse-neon/silecs
mput $1
quit
EOF

}

for file in features/*
do
    copyFileToWebDav $file
done

for file in plugins/*
do
    copyFileToWebDav $file
done

copyFileToWebDav artifacts.jar
copyFileToWebDav content.jar
copyFileToWebDav index.html
copyFileToWebDav site.xml